/**
 * 
 */
package com.github.atrow.arietta.event;

import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class SimpleSynchronousEventBusTest {
    
    private boolean testingFlag;
    
    @Test
    public void test() {
        SimpleSynchronousEventBus bus = new SimpleSynchronousEventBus();
        TestEvent event = new TestEvent();
        TestEvent.Listener listener = event.getNewListener();

        testingFlag = false;
        
        bus.registerEventListener(listener);        
        bus.fire(event);
        
        assertTrue("The flag wasn't switced by TestEvent", testingFlag);
    }
    
    
    
    private class TestEvent implements Event {

        void switchTestingFlag() {
            testingFlag = !testingFlag;
        }
        
        Listener getNewListener() {
            return new Listener();
        }

        class Listener extends AbstractEventListener<TestEvent> {

            @Override
            public boolean canHandle(Event event) {
                return (event instanceof TestEvent);
            }

            @Override
            public void handle(TestEvent event) {
                event.switchTestingFlag();
            }

        }
    }

}
