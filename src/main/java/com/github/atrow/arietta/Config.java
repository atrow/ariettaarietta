package com.github.atrow.arietta;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.prefs.Preferences;

import com.github.atrow.arietta.exception.InvalidSettingException;
import com.github.atrow.arietta.ui.UI;

/**
 * Loads and stores application config
 */
public class Config {

    /**
     * Instance of WindowConfig
     */
    private static Config instance;

    /**
     * Java Preferences to work with system default preferences system
     */
    private Preferences preferences;

    /**
     * Inner class for password encryption
     */
    private Encryptor encryptor = new Encryptor();

    /**
     * URL to aria2 server
     */
    private URL rpcURL;

    /**
     * RPC username
     */
    private String rpcUser;

    /**
     * RPC password, unencrypted
     */
    private String rpcPass;

    /**
     * Interval in seconds, how often info on downloads should be updated
     */
    private long updateInterval;

    /**
     * Should the URL from buffer be automatically pasted to AddURL dialog
     */
    private boolean addURLAutoPaste;

    /**
     * Separate structure for GUI configuration
     * TODO: Remake it
     */
    private UI.WindowConfig interfaceWindowConfig;

    /**
     * Loads config from system storage
     * @return True if all info was successfully retrieved, otherwise - false
     */
    public void fetchPreferences() {
        try {
            updateInterval = preferences.getLong("updateinterval", 3);
            rpcUser = preferences.get("rpcuser", "");
            rpcPass = encryptor.decryptString( preferences.get("rpcpass", "") );
            addURLAutoPaste = preferences.getBoolean("aurl.autopaste", false);

            interfaceWindowConfig.getWindow().setHeight( preferences.getInt("window.height", 400) );
            interfaceWindowConfig.getWindow().setWidth( preferences.getInt("window.width", 500) );
            interfaceWindowConfig.getWindow().setX( preferences.getInt("window.x", 0) );
            interfaceWindowConfig.getWindow().setY( preferences.getInt("window.y", 0) );
            interfaceWindowConfig.getWindow().setHorizontalSplit( preferences.getInt("window.hs", 110) );
            interfaceWindowConfig.getWindow().setVerticalSplit( preferences.getInt("window.vs", 180) );

            String s = preferences.get("rpcurl", "");
            if (!s.equals("")) {
                try {
                    rpcURL = new URL(s);
                } catch (MalformedURLException e) {
                    rpcURL = null;
                }
            } else {
                rpcURL = null;
            }

        } catch (Throwable e) {
            //TODO: Add detailed exception handling
            Arietta.handleException(e);
        }

    }

    /**
     * Puts config to system storage
     * @return True if all info was successfully stored, otherwise - false
     */
    public void pushPreferences() {
        try {
            if (rpcURL == null) {
                preferences.put("rpcurl", "");
            } else {
                preferences.put("rpcurl", rpcURL.toString());
            }
            preferences.putLong("updateinterval", updateInterval);
            preferences.put("rpcuser", rpcUser);
            preferences.put("rpcpass", encryptor.encryptString(rpcPass));
            preferences.putBoolean("aurl.autopaste", addURLAutoPaste);

            preferences.putInt("window.height", interfaceWindowConfig.getWindow().getHeight());
            preferences.putInt("window.width", interfaceWindowConfig.getWindow().getWidth());
            preferences.putInt("window.x", interfaceWindowConfig.getWindow().getX());
            preferences.putInt("window.y", interfaceWindowConfig.getWindow().getY());
            preferences.putInt("window.hs", interfaceWindowConfig.getWindow().getHorizontalSplit());
            preferences.putInt("window.vs", interfaceWindowConfig.getWindow().getVerticalSplit());

        } catch (Throwable e) {
            //TODO: Add detailed exception handling
            Arietta.handleException(e);
        }
    }

    //
    // Getters
    //
    /**
     * @return URL to aria2 server
     */
    public URL getRpcURL() {
        return rpcURL;
    }

    /**
     * @return Host part of RPC URL
     */
    public String getHost() {
        if (rpcURL != null) {
            return rpcURL.getHost();
        } else {
            return "";
        }
    }

    /**
     * @return Port part of RPC URL
     */
    public int getPort() {
        if (rpcURL != null) {
            return rpcURL.getPort();
        } else {
            return 0;
        }
    }

    /**
     * @return RPC Username
     */
    public String getRpcUser() {
        return rpcUser;
    }

    /**
     * @return RPC Password, unencrypted
     */
    public String getRpcPass() {
        return rpcPass;
    }

    /**
     * @return Update interval, seconds
     */
    public long getUpdateInterval() {
        return updateInterval;
    }

    /**
     * @return Update interval, seconds
     */
    public boolean getAddURLAutoPaste() {
        return addURLAutoPaste;
    }

    /**
     * @return Config structure for Interface
     */
    public UI.WindowConfig getInterfaceWindowConfig() {
        return interfaceWindowConfig;
    }

    //
    // Setters
    //

    /**
     * @param rpcURL URL to aria2 server
     * @throws InvalidSettingException Throws when rpcURL is null
     */
    public void setRpcURL(URL rpcURL) throws InvalidSettingException {
        if (rpcURL != null) {
            this.rpcURL = rpcURL;
        } else {
            throw new InvalidSettingException("Invalid rpcURL");
        }
    }

    /**
     * Sets RPC URL based on Host and Port
     * @param host Host name or IP
     * @param port Port number
     * @throws InvalidSettingException Throws when URL cannot be built with this host/port values
     */
    public void setRpcURL(String host, int port) throws InvalidSettingException {
        try {
            rpcURL = new URL("http", host, port, "/rpc");
        } catch (MalformedURLException e) {
            throw new InvalidSettingException("Invalid Server / Port");
        }
    }

    /**
     * @param rpcUser RPC Username
     * @throws InvalidSettingException Throws when rpcUser is null
     */
    public void setRpcUser(String rpcUser) throws InvalidSettingException {
        if (rpcUser != null) {
            this.rpcUser = rpcUser;
        } else {
            throw new InvalidSettingException("Invalid User");
        }
    }

    /**
     * @param addURLAutoPaste True, if autopasting is enabled
     */
    public void setAddURLAutoPaste(boolean addURLAutoPaste) {
        this.addURLAutoPaste = addURLAutoPaste;
    }

    /**
     * @param interfaceWindowConfig WindowConfig structure for Interface
     */
    public void setInterfaceWindowConfig(UI.WindowConfig interfaceWindowConfig) {
        this.interfaceWindowConfig = interfaceWindowConfig;
    }

    /**
     * @param updateInterval Update interval, seconds
     */
    public void setUpdateInterval(long updateInterval) {
        this.updateInterval = updateInterval;
    }

    /**
     * @param rpcPass RPC Password, unencrypted
     * @throws InvalidSettingException Throws when rpcPass is null
     */
    public void setRpcPass(String rpcPass) throws InvalidSettingException {
        if (rpcPass != null) {
            this.rpcPass = rpcPass;
        } else {
            throw new InvalidSettingException("Invalid Password");
        }
    }

    //
    // Constructor
    //
    private Config() {
        //TODO: IMPORTANT! Catch all exceptions of Preferences
        try {
            Preferences root = Preferences.userRoot();
            preferences = root.node("arietta");
            interfaceWindowConfig = new UI.WindowConfig();

            fetchPreferences();

        } catch (SecurityException se) {
            Arietta.onPreferencesStorageDenied();
        } catch (IllegalArgumentException iae) {
            throw iae;
        } catch (IllegalStateException ise) {
            throw ise;
        }
    }

    public static synchronized Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    /**
     * Very primitive XOR encryption, but I don't think that such type of application should use AES or similar
     */
    private class Encryptor {

        /**
         * Key for XOR encryption
         */
        private String key = "Encrypt";

        /**
         * Encrypt specified string
         * @param str Open string
         * @return Encrypted string
         */
        private String encryptString(String str) {
            StringBuffer sb = new StringBuffer (str);

            int lenStr = str.length();
            int lenKey = key.length();

            for (int i=0, j=0; i < lenStr; i++, j++ ) {
                if (j >= lenKey) j = 0;
                sb.setCharAt(i, (char)(str.charAt(i) ^ key.charAt(j)));
            }

            return sb.toString();
        }

        /**
         * Decrypt specified string
         * @param str Encrypted string
         * @return Open string
         */
        private String decryptString(String str) {
            return encryptString(str);
        }
    }
}

