package com.github.atrow.arietta;

import org.slf4j.LoggerFactory;

/**
 * Singleton wrapper for logger
 */
public class Logger {
    private static org.slf4j.Logger instance;

    private Logger() {}

    public static synchronized org.slf4j.Logger getInstance() {
        if (instance == null) {
            instance = LoggerFactory.getLogger("Arietta");
        }
        return instance;
    }
}
