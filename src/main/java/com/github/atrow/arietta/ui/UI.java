package com.github.atrow.arietta.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.MediaTracker;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.AbstractTableModel;

import com.github.atrow.arietta.Config;
import com.github.atrow.arietta.DownloadManager;
import com.github.atrow.arietta.Logger;
import com.github.atrow.arietta.StateFilter;
import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.EventBus;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.event.EventListener;
import com.github.atrow.arietta.event.impl.ConnectionEstablishedEvent;
import com.github.atrow.arietta.event.impl.ConnectionLostEvent;
import com.github.atrow.arietta.event.impl.DownloadsUpdatedEvent;
import com.github.atrow.arietta.event.impl.QueueFullEvent;
import com.github.atrow.arietta.event.impl.QueueReadyEvent;
import com.github.atrow.arietta.event.impl.StateFilterChangedEvent;
import com.github.atrow.arietta.task.TaskManager;
import com.github.atrow.arietta.ui.controller.AddUrlDialogController;
import com.github.atrow.arietta.ui.view.ErrorDialog;
import com.github.atrow.arietta.ui.view.QueueStuckDialog;
import com.github.atrow.arietta.ui.view.SettingsDialog;

/**
 * This class implements everything needed for interaction with user
 */
public class UI {
    private JPanel mainPanel;
    private JToolBar mainToolbar;
    private JList statesList;
    private JTabbedPane tabbedPane1;
    private JTable downloadsTable;
    private JButton addHTTPButton;
    private JButton pauseAllButton;
    private JButton unpauseAllButton;
    private JButton removeButton;
    private JButton unpauseButton;
    private JButton pauseButton;
    private JButton optionsButton;
    private JButton connectButton;
    private JButton settingsButton;
    private JSplitPane verticalSplit;
    private JSplitPane horizontalSplit;
    private JButton addFileButton;
    private JLabel totalLengthLabel;
    private JLabel gidLabel;
    private JLabel completedLengthLabel;
    private JLabel uploadLengthLabel;
    private JLabel downloadSpeedLabel;
    private JLabel uploadSpeedLabel;
    private JLabel connectionsLabel;
    private JLabel dirLabel;
    private JTable logTable;
    private JLabel nameLabel;
    private JLabel creationDateLabel;
    private JLabel numSeedersLabel;
    private JTable filesTable;

    private JFileChooser fileChooser;
    private JFrame window;
    private JMenuBar menu;

    private Locale locale;
    private WindowConfig windowConfig;

    private AddUrlDialogController addUrlDialogController;
    private QueueStuckDialog stuckDialog;
    private SettingsDialog settingsDialog;
    private ErrorDialog errorDialog;
    
    private final EventBus eventBus = EventBusFactory.getDefault();
    private final TaskManager taskManager = TaskManager.getInstance();
    private final DownloadManager downloadManager = DownloadManager.getInstance();

    /**
     * Constructor
     */
    public UI() {
        
        setupEventListeners();
        
        try {   //"com.sun.java.swing.plaf.windows.WindowsLookAndFeel"
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        
        downloadsTable = new ZebraJTable();
        mainPanel = new JPanel();
        mainToolbar = new JToolBar();
        mainToolbar.setBorderPainted(true);
        mainToolbar.setFloatable(false);
        mainToolbar.setRollover(true);
        mainToolbar.putClientProperty("JToolBar.isRollover", Boolean.TRUE);
        addHTTPButton = new JButton();
        addHTTPButton.setFocusable(false);
        addHTTPButton.setHideActionText(false);
        addHTTPButton.setIcon(new ImageIcon(getClass().getResource("/img/AddHTTP/8.png")));
        addHTTPButton.setText("");
        addHTTPButton.setToolTipText("Add HTTP...");
        mainToolbar.add(addHTTPButton);
        addFileButton = new JButton();
        addFileButton.setFocusable(false);
        addFileButton.setIcon(new ImageIcon(getClass().getResource("/img/OpenFile/18_3.png")));
        addFileButton.setText("");
        addFileButton.setToolTipText("Add File...");
        mainToolbar.add(addFileButton);
        final JToolBar.Separator toolBar$Separator1 = new JToolBar.Separator();
        mainToolbar.add(toolBar$Separator1);
        unpauseAllButton = new JButton();
        unpauseAllButton.setFocusable(false);
        unpauseAllButton.setIcon(new ImageIcon(getClass().getResource("/img/StartAll/12.png")));
        unpauseAllButton.setText("");
        unpauseAllButton.setToolTipText("Run all");
        mainToolbar.add(unpauseAllButton);
        pauseAllButton = new JButton();
        pauseAllButton.setFocusable(false);
        pauseAllButton.setIcon(new ImageIcon(getClass().getResource("/img/StopAll/11.png")));
        pauseAllButton.setText("");
        pauseAllButton.setToolTipText("Stop all");
        mainToolbar.add(pauseAllButton);
        final JToolBar.Separator toolBar$Separator2 = new JToolBar.Separator();
        mainToolbar.add(toolBar$Separator2);
        unpauseButton = new JButton();
        unpauseButton.setDoubleBuffered(true);
        unpauseButton.setFocusable(false);
        unpauseButton.setHideActionText(false);
        unpauseButton.setHorizontalAlignment(0);
        unpauseButton.setIcon(new ImageIcon(getClass().getResource("/img/Start/2.png")));
        unpauseButton.setText("");
        unpauseButton.setToolTipText("Start");
        mainToolbar.add(unpauseButton);
        pauseButton = new JButton();
        pauseButton.setFocusable(false);
        pauseButton.setHideActionText(false);
        pauseButton.setIcon(new ImageIcon(getClass().getResource("/img/Stop/1.png")));
        pauseButton.setText("");
        pauseButton.setToolTipText("Stop");
        mainToolbar.add(pauseButton);
        removeButton = new JButton();
        removeButton.setFocusable(false);
        removeButton.setIcon(new ImageIcon(getClass().getResource("/img/Delete/3.png")));
        removeButton.setText("");
        removeButton.setToolTipText("Delete");
        mainToolbar.add(removeButton);
        final JToolBar.Separator toolBar$Separator3 = new JToolBar.Separator();
        mainToolbar.add(toolBar$Separator3);
        optionsButton = new JButton();
        optionsButton.setFocusable(false);
        optionsButton.setIcon(new ImageIcon(getClass().getResource("/img/Options/16.png")));
        optionsButton.setText("");
        optionsButton.setToolTipText("Options");
        mainToolbar.add(optionsButton);
        
        Component horizontalGlue = Box.createHorizontalGlue();
        mainToolbar.add(horizontalGlue);
        connectButton = new JButton();
        connectButton.setFocusable(false);
        connectButton.setIcon(new ImageIcon(getClass().getResource("/img/ServerConnect/17.png")));
        connectButton.setText("");
        connectButton.setToolTipText("Connect to aria2 server");
        mainToolbar.add(connectButton);
        settingsButton = new JButton();
        settingsButton.setFocusable(false);
        settingsButton.setIcon(new ImageIcon(getClass().getResource("/img/Settings/7.png")));
        settingsButton.setText("");
        settingsButton.setToolTipText("Settings");
        mainToolbar.add(settingsButton);
        final JPanel panel1 = new JPanel();
        panel1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        FlowLayout flowLayout = (FlowLayout) panel1.getLayout();
        flowLayout.setAlignment(FlowLayout.LEFT);
        final JLabel label1 = new JLabel();
        label1.setText("The very first alpha version");
        panel1.add(label1);
        verticalSplit = new JSplitPane();
        verticalSplit.setContinuousLayout(true);
        verticalSplit.setDividerLocation(180);
        verticalSplit.setOneTouchExpandable(false);
        verticalSplit.setOrientation(0);
        verticalSplit.setResizeWeight(0.0);
        horizontalSplit = new JSplitPane();
        horizontalSplit.setBackground(new Color(-1));
        horizontalSplit.setDividerLocation(100);
        horizontalSplit.setEnabled(true);
        verticalSplit.setLeftComponent(horizontalSplit);
        statesList = new JList();
        statesList.setBackground(SystemColor.control);
        statesList.setFont(UIManager.getFont("Menu.acceleratorFont"));
        final DefaultListModel defaultListModel1 = new DefaultListModel();
        statesList.setModel(defaultListModel1);
        horizontalSplit.setLeftComponent(statesList);
        final JScrollPane scrollPane1 = new JScrollPane();
        scrollPane1.setBackground(new Color(-1));
        scrollPane1.setEnabled(true);
        horizontalSplit.setRightComponent(scrollPane1);
        downloadsTable.setFillsViewportHeight(false);
        downloadsTable.setShowHorizontalLines(false);
        downloadsTable.setShowVerticalLines(false);
        scrollPane1.setViewportView(downloadsTable);
        tabbedPane1 = new JTabbedPane();
        verticalSplit.setRightComponent(tabbedPane1);
        final JPanel panel2 = new JPanel();
        tabbedPane1.addTab("General", panel2);
        gidLabel = new JLabel();
        gidLabel.setText("GID:");
        totalLengthLabel = new JLabel();
        totalLengthLabel.setText("totalLength:");
        completedLengthLabel = new JLabel();
        completedLengthLabel.setText("completedLength:");
        uploadLengthLabel = new JLabel();
        uploadLengthLabel.setText("uploadLength:");
        downloadSpeedLabel = new JLabel();
        downloadSpeedLabel.setText("downloadSpeed:");
        uploadSpeedLabel = new JLabel();
        uploadSpeedLabel.setText("uploadSpeed:");
        connectionsLabel = new JLabel();
        connectionsLabel.setText("connections:");
        dirLabel = new JLabel();
        dirLabel.setText("dir:");
        GroupLayout gl_panel2 = new GroupLayout(panel2);
        gl_panel2.setHorizontalGroup(
        	gl_panel2.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel2.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_panel2.createParallelGroup(Alignment.LEADING)
        				.addComponent(uploadLengthLabel)
        				.addGroup(gl_panel2.createSequentialGroup()
        					.addGroup(gl_panel2.createParallelGroup(Alignment.LEADING)
        						.addComponent(gidLabel)
        						.addComponent(totalLengthLabel)
        						.addComponent(completedLengthLabel))
        					.addGap(167)
        					.addGroup(gl_panel2.createParallelGroup(Alignment.LEADING)
        						.addComponent(connectionsLabel)
        						.addComponent(uploadSpeedLabel)
        						.addGroup(gl_panel2.createSequentialGroup()
        							.addComponent(downloadSpeedLabel)
        							.addGap(160)
        							.addComponent(dirLabel)))))
        			.addContainerGap(232, Short.MAX_VALUE))
        );
        gl_panel2.setVerticalGroup(
        	gl_panel2.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel2.createSequentialGroup()
        			.addGap(5)
        			.addGroup(gl_panel2.createParallelGroup(Alignment.LEADING)
        				.addComponent(gidLabel)
        				.addGroup(gl_panel2.createParallelGroup(Alignment.BASELINE)
        					.addComponent(downloadSpeedLabel)
        					.addComponent(dirLabel)))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(gl_panel2.createParallelGroup(Alignment.LEADING)
        				.addGroup(gl_panel2.createSequentialGroup()
        					.addComponent(totalLengthLabel)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(completedLengthLabel)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(uploadLengthLabel))
        				.addGroup(gl_panel2.createSequentialGroup()
        					.addComponent(uploadSpeedLabel)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(connectionsLabel)))
        			.addContainerGap(17, Short.MAX_VALUE))
        );
        panel2.setLayout(gl_panel2);
        final JPanel panel3 = new JPanel();
        tabbedPane1.addTab("BitTorrent", panel3);
        nameLabel = new JLabel();
        nameLabel.setText("name:");
        creationDateLabel = new JLabel();
        creationDateLabel.setText("creationDate:");
        numSeedersLabel = new JLabel();
        numSeedersLabel.setText("numSeeders:");
        GroupLayout gl_panel3 = new GroupLayout(panel3);
        gl_panel3.setHorizontalGroup(
        	gl_panel3.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel3.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_panel3.createParallelGroup(Alignment.LEADING)
        				.addComponent(nameLabel)
        				.addComponent(creationDateLabel)
        				.addComponent(numSeedersLabel))
        			.addContainerGap(675, Short.MAX_VALUE))
        );
        gl_panel3.setVerticalGroup(
        	gl_panel3.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel3.createSequentialGroup()
        			.addGap(5)
        			.addComponent(nameLabel)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(creationDateLabel)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(numSeedersLabel)
        			.addContainerGap(37, Short.MAX_VALUE))
        );
        panel3.setLayout(gl_panel3);
        final JPanel panel4 = new JPanel();
        tabbedPane1.addTab("Files", panel4);
        filesTable = new JTable();
        panel4.add(filesTable);
        final JPanel panel5 = new JPanel();
        tabbedPane1.addTab("Log", panel5);
        logTable = new JTable();
        panel5.add(logTable);

        
        
        mainPanel.repaint();

        window = new JFrame();
        menu = new JMenuBar();

        windowConfig = Config.getInstance().getInterfaceWindowConfig();

        fileChooser = new JFileChooser();

        fileChooser.setCurrentDirectory(new File("."));
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return (f.getName().toLowerCase().endsWith(".torrent") ||
                        f.getName().toLowerCase().endsWith(".metalink"));
            }

            @Override
            public String getDescription() {
                return "Torrent or Metalink files";
            }
        });

        addUrlDialogController = new AddUrlDialogController();
        stuckDialog = new QueueStuckDialog();
        settingsDialog = new SettingsDialog();
        errorDialog = new ErrorDialog();

        window.setTitle("Arietta");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setContentPane(mainPanel);
        window.setJMenuBar(menu);

        window.setBounds(
                windowConfig.getWindow().getX(),
                windowConfig.getWindow().getY(),
                windowConfig.getWindow().getWidth(),
                windowConfig.getWindow().getHeight()
        );

        verticalSplit.setDividerLocation(windowConfig.getWindow().getVerticalSplit());
        horizontalSplit.setDividerLocation(windowConfig.getWindow().getHorizontalSplit());

        String[] list = {"All", "Running", "Stopped", "Done"};
        statesList.setListData(list);
        statesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        statesList.setSelectedIndex(0);

        setLocale("RU");
        rebuildMenu();

        SwingUtilities.updateComponentTreeUI(window);

        addHTTPButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddUrl();
            }
        });

        pauseAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onPauseAll();
            }
        });

        unpauseAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onUnpauseAll();
            }
        });

        downloadsTable.setModel(new DownloadsTableModel());
        GroupLayout gl_mainPanel = new GroupLayout(mainPanel);
        gl_mainPanel.setHorizontalGroup(
        	gl_mainPanel.createParallelGroup(Alignment.LEADING)
        		.addComponent(mainToolbar, GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE)
        		.addComponent(panel1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE)
        		.addComponent(verticalSplit, GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE)
        );
        gl_mainPanel.setVerticalGroup(
        	gl_mainPanel.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_mainPanel.createSequentialGroup()
        			.addComponent(mainToolbar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addGap(3)
        			.addComponent(verticalSplit, GroupLayout.PREFERRED_SIZE, 364, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(panel1, GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
        );
        mainPanel.setLayout(gl_mainPanel);

        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onRemove();
            }
        });

        unpauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onUnpause();
            }
        });

        pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onPause();
            }
        });

        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventBusFactory.getDefault().fire(new ConnectionEstablishedEvent());
            }
        });

        downloadsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                updateDlInfo();
            }
        });

        downloadsTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if ((e.getClickCount() == 2) && (!downloadsTable.getSelectionModel().isSelectionEmpty())) {
                    int id = downloadsTable.convertRowIndexToModel(downloadsTable.getSelectedRow());
                    Integer gid = (Integer) downloadsTable.getModel().getValueAt(id, 0);
                    taskManager.getOptions(gid);
                }
            }
        });

        window.addWindowStateListener(new WindowStateListener() {
            @Override
            public void windowStateChanged(WindowEvent e) {
                //if (e.getNewState() == WindowEvent.WINDOW_OPENED)
                //    Arietta.onIfaceReady();
            }
        });

        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showSettings();
            }
        });

        addFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int res = fileChooser.showDialog(window, "Open");

                if (res == JFileChooser.APPROVE_OPTION) {
                    String f = fileChooser.getSelectedFile().getPath();
                    DownloadManager.getInstance().addDownloadFromFileName(f);
                }
            }
        });

        window.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                //Arietta.onIfaceReady();
            }

            @Override
            public void windowClosing(WindowEvent e) {
                windowConfig.push();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        window.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                windowConfig.getWindow().setHeight(e.getComponent().getHeight());
                windowConfig.getWindow().setWidth(e.getComponent().getWidth());

                //TODO: is it really needed?
                super.componentResized(e);
            }

            @Override
            public void componentMoved(ComponentEvent e) {
                windowConfig.getWindow().setX(e.getComponent().getX());
                windowConfig.getWindow().setY(e.getComponent().getY());
                super.componentResized(e);
            }
        });

        horizontalSplit.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                windowConfig.getWindow().setVerticalSplit(verticalSplit.getDividerLocation());
                Config.getInstance().pushPreferences();
                super.componentResized(e);
            }
        });

        statesList.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                windowConfig.getWindow().setHorizontalSplit(horizontalSplit.getDividerLocation());
                Config.getInstance().pushPreferences();
                super.componentResized(e);
            }
        });

        statesList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                ArrayList<String> list = new ArrayList<String>();
                Object[] oArray = statesList.getSelectedValues();
                for (Object o : oArray) {
                    list.add((String) o);
                }

                StateFilter newStateFilter = null;
                
                if (list.contains("All")) {
                    newStateFilter = StateFilter.getStateFilterAll();
                } else {
                    //TODO: IMPORTANT! this is temporary, but it MUST be removed
                    if (list.contains("Running")) {
                	newStateFilter = StateFilter.getStateFilterRunning();
                    }
                    if (list.contains("Stopped")) {
                	newStateFilter = StateFilter.getStateFilterStopped();
                    }
                    if (list.contains("Done")) {
                	newStateFilter = StateFilter.getStateFilterDone();
                    }
                }
                
                eventBus.fire( new StateFilterChangedEvent(newStateFilter) );
            }
        });

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                window.setVisible(true);
            }
        });

    }

    private void setupEventListeners() {
        EventListener<StateFilterChangedEvent> stateFilterChangedEventListener = new StateFilterChangedEvent.Listener() {
            
            @Override
            public void handle(StateFilterChangedEvent event) {
                update();                
            }
        };
        
        EventListener<DownloadsUpdatedEvent> downloadsUpdatedEventListener = new DownloadsUpdatedEvent.Listener() {
            
            @Override
            public void handle(DownloadsUpdatedEvent event) {
                update();
            }
        }; 
        
        eventBus.registerEventListener(new QueueStuckEventListener());
        eventBus.registerEventListener(new QueueReadyEventListener());
        eventBus.registerEventListener(new ConnectionLostEventListener());
        eventBus.registerEventListener(new ConnectionEstablishedEventListener());
        
        eventBus.registerEventListener(stateFilterChangedEventListener);
        eventBus.registerEventListener(downloadsUpdatedEventListener);
        
    }

    //
    // Public static methods
    //
    public static String getHumanSize(Long size) {

        if (size == null) {
            return "";
        }

        String s = "";
        double num = 0;
        if (size < 1024) {
            num = size;

        } else if (size < (1024 * 1024)) {
            num = (double) size / 1024;
            s = "Kb";

        } else if (size < (1024 * 1024 * 1024)) {
            num = (double) size / (1024 * 1024);
            s = "Mb";

        } else {
            num = (double) size / (1024 * 1024 * 1024);
            s = "Gb";
        }

        DecimalFormat oneDec = new DecimalFormat("0.0");

        return oneDec.format(num) + " " + s;
    }

    public static String getHumanSpeed(Long speed) {

        if (speed == null) {
            return "";
        }

        String s = "";
        double num = 0;
        if (speed < 1024) {
            num = speed;

        } else if (speed < (1024 * 1024)) {
            num = (double) speed / 1024;
            s = "Kb/s";

        } else if (speed < (1024 * 1024 * 1024)) {
            num = (double) speed / (1024 * 1024);
            s = "Mb/s";

        } else {
            num = (double) speed / (1024 * 1024 * 1024);
            s = "Gb/s";
        }

        DecimalFormat oneDec = new DecimalFormat("0.0");

        return oneDec.format(num) + " " + s;
    }

    public static String convertToHTML(String string) {
        return "<html>" + string.replaceAll("\n", "<br>");
    }

    //
    // Public methods
    //
    public void setLocale(String l) {
        locale = new Locale(l);
        Locale.setDefault(locale);
    }

    public void showError(final ErrorDialog.Callback callback, final String title, final String message, final ErrorDialog.ButtonSet buttonSet) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
            	errorDialog.setCallback(callback);
            	errorDialog.show(title, message, buttonSet);
            }
        });
    }

    public void showSettings() {
        settingsDialog.pack();
        settingsDialog.setLocationRelativeTo(window);
        settingsDialog.setVisible(true);
    }

    //TODO: Make something sane instead of this
    public void updateDlInfo() {
        if (!downloadsTable.getSelectionModel().isSelectionEmpty()) {
            int id = downloadsTable.convertRowIndexToModel(downloadsTable.getSelectedRow());
            Integer gid = (Integer) downloadsTable.getModel().getValueAt(id, 0);
            Download dl = DownloadManager.getInstance().getDownloadsMap().get(gid);

            totalLengthLabel.setText("totalLength: " + getHumanSize(dl.getTotalLength()));
            gidLabel.setText("GID: " + dl.getGid());
            completedLengthLabel.setText("completedLength: " + getHumanSize(dl.getCompletedLength()));
            uploadLengthLabel.setText("uploadLength: " + getHumanSize(dl.getUploadLength()));
            downloadSpeedLabel.setText("downloadSpeed: " + getHumanSpeed(dl.getDownloadSpeed()));
            uploadSpeedLabel.setText("uploadSpeed: " + getHumanSpeed(dl.getUploadSpeed()));
            connectionsLabel.setText("connections: " + dl.getConnections());
            dirLabel.setText("dir: " + dl.getDir());
            nameLabel.setText("nameLabel: " + dl.getName());
            creationDateLabel.setText("creationDate: ");
            numSeedersLabel.setText("numSeedersLabel: ");

        } else {
            totalLengthLabel.setText("totalLength: ");
            gidLabel.setText("GID: ");
            completedLengthLabel.setText("completedLength: ");
            uploadLengthLabel.setText("uploadLengthLabel: ");
            downloadSpeedLabel.setText("downloadSpeedLabel: ");
            uploadSpeedLabel.setText("uploadSpeed: ");
            connectionsLabel.setText("connections: ");
            dirLabel.setText("dir: ");
            nameLabel.setText("nameLabel: ");
            creationDateLabel.setText("creationDate: ");
            numSeedersLabel.setText("numSeedersLabel: ");
        }
    }

    public void update() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                downloadsTable.repaint();
                downloadsTable.updateUI();
                updateDlInfo();
            }
        });
    }

    //
    // Event methods
    //
    public void onAddUrl() {
        addUrlDialogController.show();
    }

    public void onPauseAll() {
        taskManager.pauseAll();
    }

    public void onUnpauseAll() {
        taskManager.unpauseAll();
    }

    private void onRemove() {
        if (!downloadsTable.getSelectionModel().isSelectionEmpty()) {
            //int row = downloadsTable.getSelectedRow();
            int id = downloadsTable.convertRowIndexToModel(downloadsTable.getSelectedRow());
            Integer gid = (Integer) downloadsTable.getModel().getValueAt(id, 0);
            downloadManager.removeDownload(gid);
        }
    }

    private void onUnpause() {
        if (!downloadsTable.getSelectionModel().isSelectionEmpty()) {
            int id = downloadsTable.convertRowIndexToModel(downloadsTable.getSelectedRow());
            Integer gid = (Integer) downloadsTable.getModel().getValueAt(id, 0);
            downloadManager.unpauseDownload(gid);
        }
    }

    private void onPause() {
        if (!downloadsTable.getSelectionModel().isSelectionEmpty()) {
            int id = downloadsTable.convertRowIndexToModel(downloadsTable.getSelectedRow());
            Integer gid = (Integer) downloadsTable.getModel().getValueAt(id, 0);
            downloadManager.pauseDownload(gid);
        }
    }

    private class QueueStuckEventListener extends QueueFullEvent.Listener {

        @Override
        public void handle(QueueFullEvent event) {
            
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    stuckDialog.setMessage("Queue is full. Wait or close application...");
                    stuckDialog.pack();
                    stuckDialog.setVisible(true);
                }
            });
        }        
    }
    
    private class QueueReadyEventListener extends QueueReadyEvent.Listener {

        @Override
        public void handle(QueueReadyEvent event) {

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    stuckDialog.setLocationRelativeTo(window);
                    stuckDialog.setVisible(false);
                }
            });
        }
    }

    private class ConnectionLostEventListener extends ConnectionLostEvent.Listener {
        @Override
        public void handle(ConnectionLostEvent event) {

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    addHTTPButton.setEnabled(false);
                    addFileButton.setEnabled(false);
                    pauseButton.setEnabled(false);
                    unpauseButton.setEnabled(false);
                    removeButton.setEnabled(false);
                    pauseAllButton.setEnabled(false);
                    unpauseAllButton.setEnabled(false);
                    optionsButton.setEnabled(false);
                    connectButton.setEnabled(true);
                    update();
                }
            });
        }
    }

    private class ConnectionEstablishedEventListener extends ConnectionEstablishedEvent.Listener {
        @Override
        public void handle(ConnectionEstablishedEvent event) {

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    addHTTPButton.setEnabled(true);
                    addFileButton.setEnabled(true);
                    pauseButton.setEnabled(true);
                    unpauseButton.setEnabled(true);
                    removeButton.setEnabled(true);
                    pauseAllButton.setEnabled(true);
                    unpauseAllButton.setEnabled(true);
                    optionsButton.setEnabled(true);
                    connectButton.setEnabled(false);
                }
            });
        }
    }
        
    //WTF???
    //
    // Private methods
    //
    private void rebuildMenu() {

        Logger.getInstance().info(ResourceBundle.getBundle("lang", locale).getString("File"));
        Logger.getInstance().info(ResourceBundle.getBundle("lang", locale).getString("Edit"));

        JMenu fileMenu = new JMenu(ResourceBundle.getBundle("lang", locale).getString("File"));
        JMenu editMenu = new JMenu(ResourceBundle.getBundle("lang", locale).getString("Edit"));

/*
        JMenuItem newItem = new myJMenuItem("New", Jipad.newListener, "/img/16/File-New-icon.png", 'N');

        JMenuItem openItem = new myJMenuItem("Open", Jipad.openListener, "/img/16/My-Documents-icon.png", 'O');

        JMenuItem saveItem = new myJMenuItem("Save", Jipad.saveListener, "/img/16/Save-icon.png", 'S');

        JMenuItem saveAsItem = new myJMenuItem("Save_As", Jipad.saveAsListener);

        JMenuItem exitItem = new myJMenuItem("Exit", Jipad.exitListener);

        fileMenu.add(newItem);
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(saveAsItem);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);

        JMenuItem cutItem = new myJMenuItem("Cut", Jipad.cutListener, null, 'X');
        JMenuItem copyItem = new myJMenuItem("Copy", Jipad.copyListener, "/img/16/Document-Copy-icon.png", 'C');
        JMenuItem pasteItem = new myJMenuItem("Paste", Jipad.pasteListener, "/img/16/Clipboard-Paste-icon.png", 'V');

        editMenu.add(cutItem);
        editMenu.add(copyItem);
        editMenu.add(pasteItem);
  */
        menu.removeAll();
        menu.add(fileMenu);
        menu.add(editMenu);
    }

    //
    // Public classes
    //
    //TODO: IMPORTANT! Total ugliness. Remake.
    public static class WindowConfig {

        private final Window window = new Window();

        public Window getWindow() {
            return window;
        }

        private void push() {
            Config.getInstance().setInterfaceWindowConfig(this);
            Config.getInstance().pushPreferences();
        }

        public class Window {
            private int x;
            private int y;
            private int width;
            private int height;
            private int verticalSplit;
            private int horizontalSplit;

            public int getX() {
                return x;
            }

            public int getY() {
                return y;
            }

            public int getWidth() {
                return width;
            }

            public int getHeight() {
                return height;
            }

            public int getVerticalSplit() {
                return verticalSplit;
            }

            public int getHorizontalSplit() {
                return horizontalSplit;
            }

            public void setX(int x) {
                this.x = x;
            }

            public void setY(int y) {
                this.y = y;
            }

            public void setWidth(int width) {
                this.width = width;
            }

            public void setHeight(int height) {
                this.height = height;
            }

            public void setVerticalSplit(int verticalSplit) {
                this.verticalSplit = verticalSplit;
            }

            public void setHorizontalSplit(int horizontalSplit) {
                this.horizontalSplit = horizontalSplit;
            }
        }

    }

    //
    // Private classes
    //
    private class myJMenuItem extends JMenuItem {

        myJMenuItem(String localizedName) {
            super(ResourceBundle.getBundle("lang", locale).getString(localizedName));
        }

        myJMenuItem(String localizedName, ActionListener listener) {
            this(localizedName);
            addActionListener(listener);
        }

        myJMenuItem(String localizedName, ActionListener listener, String iconFileName) {
            this(localizedName, listener);
            if (iconFileName != null) {
                ImageIcon icon = new ImageIcon(getClass().getResource(iconFileName));
                if (icon.getImageLoadStatus() == MediaTracker.COMPLETE) {
                    setIcon(icon);
                }
            }
        }

        myJMenuItem(String localizedName, ActionListener listener, String iconFileName, char ctrlKey) {
            this(localizedName, listener, iconFileName);
            setAccelerator(KeyStroke.getKeyStroke(ctrlKey, ActionEvent.CTRL_MASK));
        }

        public void bulk() {
            //nothing;
        }
    }

    private class DownloadsTableModel extends AbstractTableModel {
        /**
         * Returns the number of rows in the model. A
         * <code>JTable</code> uses this method to determine how many rows it
         * should display.  This method should be quick, as it
         * is called frequently during rendering.
         *
         * @return the number of rows in the model
         * @see #getColumnCount
         */
        @Override
        public int getRowCount() {
            //Logger.getInstance().debug("Table gets count of downloads: "+ Arietta.getDownloadsMap().size());
            return DownloadManager.getInstance().getDownloadsListFiltered().size();
        }

        /**
         * Returns the number of columns in the model. A
         * <code>JTable</code> uses this method to determine how many columns it
         * should create and display by default.
         *
         * @return the number of columns in the model
         * @see #getRowCount
         */
        @Override
        public int getColumnCount() {
            return cells.size();
        }

        /**
         * Returns the value for the cell at <code>columnIndex</code> and
         * <code>rowIndex</code>.
         *
         * @param rowIndex    the row whose value is to be queried
         * @param columnIndex the column whose value is to be queried
         * @return the value Object at the specified cell
         */
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            //Download dl = Arietta.getDownloadsMap().get(rowIndex+1);
            //Download[] dla = (Download[])Arietta.getDownloadsMap().values().toArray();

            List<Download> dl = DownloadManager.getInstance().getDownloadsListFiltered();


            //TODO: Error here, raised when deleted one download several times
            return cells.get(columnIndex).getValue(dl.get(rowIndex));

            //Logger.getInstance().debug("Table gets value for row "+ rowIndex +" col "+ columnIndex +" : "+Arietta.getDownloadsMap().get(rowIndex+1).getStatusFields().get(columnIndex));
            //dl.getStatusFields();
            //return Arietta.getDownloadsMap().get(rowIndex+1).getStatusFields().get(columnIndex);
        }

        /**
         * Returns a default name for the column using spreadsheet conventions:
         * A, B, C, ... Z, AA, AB, etc.  If <code>column</code> cannot be found,
         * returns an empty string.
         *
         * @param column the column being queried
         * @return a string containing the default name of <code>column</code>
         */
        @Override
        public String getColumnName(int column) {
            return cells.get(column).getName();
        }

        private List<Cell> cells;

        {
            cells = new ArrayList<Cell>(10);
            cells.add(new GidCell());
            //cells.add(new StatusStringCell());
            cells.add(new NameCell());
            cells.add(new TotalHumanCell());
            cells.add(new ProgressPctCell());
            cells.add(new DownloadedCell());

            cells.add(new StatusStringCell());
            cells.add(new DownloadSpeedCell());
        }

        private Object getGid(Download dl) {
            return dl.getGid();
        }

        private class Cell {
            protected String name;

            public String getName() {
                return name;
            }

            public Object getValue(Download dl) {
                return null;
            }
        }

        private class GidCell extends Cell {
            {
                name = "Gid";
            }

            @Override
            public Integer getValue(Download dl) {
                return dl.getGid();
            }
        }

        private class StatusStringCell extends Cell {
            {
                name = "Status";
            }

            @Override
            public String getValue(Download dl) {
                switch (dl.getStatus()) {
                    case NEW:
                        return "New";
                    case ACTIVE:
                        return "Active";
                    case COMPLETE:
                        return "Complete";
                    case PAUSED:
                        return "Paused";
                    case ERROR:
                        return "Error";
                    case REMOVED:
                        return "Removed";
                    case WAITING:
                        return "Waiting";
                    default:
                        return "Unknown";
                }
            }
        }

        private class NameCell extends Cell {
            {
                name = "Name";
            }

            @Override
            public String getValue(Download dl) {
                return dl.getName();
            }
        }

        private class ProgressPctCell extends Cell {
            {
                name = "Progress";
            }

            @Override
            public String getValue(Download dl) {
                if (dl.getTotalLength() == null) {
                    return "";
                } else {
                    return Math.round(((double) dl.getCompletedLength() / dl.getTotalLength()) * 100) + "%";
                }
            }
        }

        private class TotalHumanCell extends Cell {
            {
                name = "Total";
            }

            @Override
            public String getValue(Download dl) {
                return getHumanSize(dl.getTotalLength());
            }
        }

        private class DownloadedCell extends Cell {
            {
                name = "Done";
            }

            @Override
            public String getValue(Download dl) {
                return getHumanSize(dl.getCompletedLength());
            }
        }

        private class DownloadSpeedCell extends Cell {
            {
                name = "In";
            }

            @Override
            public String getValue(Download dl) {
                return getHumanSpeed(dl.getDownloadSpeed());
            }
        }

        private class UploadSpeedCell extends Cell {
            {
                name = "Out";
            }

            @Override
            public String getValue(Download dl) {
                return getHumanSpeed(dl.getUploadSpeed());
            }
        }
    }
}
