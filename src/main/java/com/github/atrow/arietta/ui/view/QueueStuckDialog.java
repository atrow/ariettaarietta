package com.github.atrow.arietta.ui.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.github.atrow.arietta.ui.UI;

public class QueueStuckDialog extends JDialog {

    private static final long serialVersionUID = 6555731087408963791L;
    private final JPanel contentPane;
    private final JButton buttonCancel;
    private final JLabel errorText;

    public QueueStuckDialog() {
        setResizable(false);

        /*
         * Begin code managed by Window builder
         */

        contentPane = new JPanel();
        buttonCancel = new JButton();
        buttonCancel.setText("Exit");

        errorText = new JLabel("New label");

        JProgressBar progressBar = new JProgressBar();
        progressBar.setIndeterminate(true);
        progressBar.setStringPainted(false);

        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                gl_contentPane
                        .createSequentialGroup()
                        .addContainerGap()
                        .addGroup(
                                gl_contentPane.createParallelGroup(Alignment.LEADING)
                                        .addComponent(errorText, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                                        .addComponent(buttonCancel, Alignment.TRAILING)
                                        .addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE))
                        .addContainerGap()));

        gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                Alignment.TRAILING,
                gl_contentPane
                        .createSequentialGroup()
                        .addContainerGap()
                        .addComponent(errorText)
                        .addPreferredGap(ComponentPlacement.RELATED)
                        .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(ComponentPlacement.RELATED, 71, Short.MAX_VALUE).addComponent(buttonCancel)
                        .addContainerGap()));

        contentPane.setLayout(gl_contentPane);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonCancel);

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        /*
         * End code managed by Window builder
         */

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onCancel() {
        System.exit(0);
    }

    public void setMessage(String message) {
        errorText.setText(UI.convertToHTML(message));
    }

}
