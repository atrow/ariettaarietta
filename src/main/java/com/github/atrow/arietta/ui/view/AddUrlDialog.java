package com.github.atrow.arietta.ui.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.github.atrow.arietta.ui.controller.AddUrlDialogController;

/**
 * Modal dialog which manages new download URL addition
 * 
 * @author AtRow
 * 
 */
public class AddUrlDialog extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 9208170514150568569L;

    /**
     *
     */
    private AddUrlDialogController controller;

    /**
     * Text box where to put download URL
     */
    private final JTextField urlTextField;

    /**
     * Pastes download URL from Clipboard
     */
    private final JButton pasteButton;

    /**
     * If checkbox is set, paste URL automatically
     */
    private final JCheckBox pasteAutomaticallyCheckBox;

    /**
     * Default constructor
     */
    public AddUrlDialog() {

        /*
         * Begin code managed by Window builder
         */

        JPanel contentPane = new JPanel();
        setContentPane(contentPane);

        final JLabel label1 = new JLabel();
        label1.setText("Specify URL:");

        urlTextField = new JTextField();

        pasteButton = new JButton();
        pasteButton.setText("Paste");
        pasteButton.setIcon(new ImageIcon(getClass().getResource("/img/Paste/18_2.png")));
        pasteButton.setToolTipText("Paste");

        pasteAutomaticallyCheckBox = new JCheckBox();
        pasteAutomaticallyCheckBox.setText("Paste automatically");

        JButton buttonOK = new JButton();
        buttonOK.setText("OK");
        getRootPane().setDefaultButton(buttonOK);

        JButton buttonCancel = new JButton();
        buttonCancel.setText("Cancel");

        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                gl_contentPane
                        .createSequentialGroup()
                        .addContainerGap()
                        .addGroup(
                                gl_contentPane
                                        .createParallelGroup(Alignment.LEADING)
                                        .addComponent(urlTextField, GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                                        .addGroup(
                                                gl_contentPane.createSequentialGroup().addComponent(pasteButton)
                                                        .addPreferredGap(ComponentPlacement.RELATED)
                                                        .addComponent(pasteAutomaticallyCheckBox))
                                        .addGroup(
                                                Alignment.TRAILING,
                                                gl_contentPane
                                                        .createSequentialGroup()
                                                        .addComponent(buttonOK, GroupLayout.PREFERRED_SIZE, 56,
                                                                GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(ComponentPlacement.RELATED)
                                                        .addComponent(buttonCancel)).addComponent(label1))
                        .addContainerGap()));
        gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                gl_contentPane
                        .createSequentialGroup()
                        .addContainerGap()
                        .addComponent(label1)
                        .addPreferredGap(ComponentPlacement.RELATED)
                        .addComponent(urlTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(ComponentPlacement.RELATED)
                        .addGroup(
                                gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(pasteButton)
                                        .addComponent(pasteAutomaticallyCheckBox))
                        .addPreferredGap(ComponentPlacement.UNRELATED)
                        .addGroup(
                                gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(buttonCancel)
                                        .addComponent(buttonOK))
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        contentPane.setLayout(gl_contentPane);
        setResizable(false);
        setModal(true);
        setTitle("Add URL");

        /*
         * End code managed by Window builder
         */

        pasteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                urlTextField.setText(controller.getPasteString());
            }
        });

        pasteAutomaticallyCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.onPasteAutomaticallyChange(pasteAutomaticallyCheckBox.isSelected());
            }
        });

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.onConfirm();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                controller.onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    }

    public void setController(AddUrlDialogController controller) {
        this.controller = controller;
    }

    public void setUrlText(String text) {
        urlTextField.setText(text);
    }

    public String getUrlText() {
        return urlTextField.getText();
    }

    public void setPasteAutomatically(boolean autoPasteEnabled) {
        pasteAutomaticallyCheckBox.setSelected(autoPasteEnabled);
    }

}
