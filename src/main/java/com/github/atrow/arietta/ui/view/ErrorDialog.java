package com.github.atrow.arietta.ui.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.github.atrow.arietta.ui.FocusTraversalOnArray;
import com.github.atrow.arietta.ui.UI;

/**
 * Shows error dialog. Can execute required actions, which should be passed
 * trough Callback interface
 * 
 * @author AtRow
 * 
 */
public class ErrorDialog extends JDialog {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1420345808138831127L;
    private final JButton buttonOK;
    private final JButton buttonCancel;
    private final JLabel errorText;
    private Callback callback;

    public static enum ButtonSet {
        OK, CANCEL, OK_CANCEL
    };

    public ErrorDialog() {

        /*
         * Begin code managed by Window builder
         */

        JPanel contentPane = new JPanel();

        setContentPane(contentPane);
        setModal(true);

        errorText = new JLabel();
        errorText.setBackground(new Color(-1));
        errorText.setEnabled(true);
        errorText.setText("<html>fd<br><br>sdf</html>");
        errorText.putClientProperty("html.disable", Boolean.FALSE);

        buttonOK = new JButton();
        buttonOK.setText("OK");
        getRootPane().setDefaultButton(buttonOK);

        buttonCancel = new JButton();
        buttonCancel.setText("Exit");

        JButton btnCopy = new JButton("copy");

        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                gl_contentPane
                        .createSequentialGroup()
                        .addContainerGap()
                        .addGroup(
                                gl_contentPane
                                        .createParallelGroup(Alignment.LEADING)
                                        .addComponent(errorText, GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                                        .addGroup(
                                                gl_contentPane.createSequentialGroup().addComponent(btnCopy)
                                                        .addGap(105).addComponent(buttonOK)
                                                        .addPreferredGap(ComponentPlacement.RELATED)
                                                        .addComponent(buttonCancel))).addContainerGap()));

        gl_contentPane
                .setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
                        .addGroup(
                                gl_contentPane
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(errorText)
                                        .addPreferredGap(ComponentPlacement.RELATED)
                                        .addGroup(
                                                gl_contentPane
                                                        .createParallelGroup(Alignment.TRAILING)
                                                        .addGroup(
                                                                gl_contentPane.createSequentialGroup()
                                                                        .addComponent(btnCopy).addGap(32))
                                                        .addGroup(
                                                                gl_contentPane
                                                                        .createSequentialGroup()
                                                                        .addGroup(
                                                                                gl_contentPane
                                                                                        .createParallelGroup(
                                                                                                Alignment.BASELINE)
                                                                                        .addComponent(buttonCancel)
                                                                                        .addComponent(buttonOK))
                                                                        .addContainerGap()))));

        contentPane.setLayout(gl_contentPane);
        contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { buttonOK, buttonCancel,
                btnCopy, errorText }));

        /*
         * End code managed by Window builder
         */

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getCallback().onOK();
                setVisible(false);
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getCallback().onCancel();
                setVisible(false);
            }
        });

        // Default callback
        setCallback(new Callback() {
            @Override
            public void onOK() {
                setVisible(false);
            }

            @Override
            public void onCancel() {
                System.exit(0);
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                getCallback().onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getCallback().onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * Sets error message. Looks like it can be in HTML
     * 
     * @param message
     */
    public void setMessage(String message) {
        errorText.setText(UI.convertToHTML(message));
    }

    // TODO: put Callback and raise to Interface
    /**
     * Show Error message
     * 
     * @param title
     * @param message
     * @param buttonSet
     */
    public void show(String title, String message, ButtonSet buttonSet) {

        switch (buttonSet) {
        case OK:
            buttonOK.setVisible(true);
            buttonCancel.setVisible(false);
            break;
        case CANCEL:
            buttonOK.setVisible(false);
            buttonCancel.setVisible(true);
            break;
        case OK_CANCEL:
            buttonOK.setVisible(true);
            buttonCancel.setVisible(true);
            break;
        }

        setTitle(title);
        setMessage(message);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * @return the callback
     */
    private Callback getCallback() {
        return callback;
    }

    /**
     * @param callback
     *            the callback to set
     */
    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    /**
     * Defines methods which should be executed when appropriate button of
     * ErrorDialog is clicked
     * 
     * @author AtRow
     * 
     */
    public interface Callback {
        void onOK();

        void onCancel();
    }
}
