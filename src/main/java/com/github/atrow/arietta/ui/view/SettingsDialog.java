package com.github.atrow.arietta.ui.view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.github.atrow.arietta.Arietta;
import com.github.atrow.arietta.Config;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.event.impl.ConfigUpdatedEvent;
import com.github.atrow.arietta.exception.InvalidSettingException;
import com.github.atrow.arietta.ui.FocusTraversalOnArray;

public class SettingsDialog extends JDialog {
    /**
	 * 
	 */
    private static final long serialVersionUID = -587973604236011226L;

    private final JPanel contentPane;
    private final JButton buttonOK;
    private final JButton buttonCancel;
    private final JTabbedPane tabbedPane;
    private final JFormattedTextField serverTextField;
    private final JTextField userTextField;
    private final JFormattedTextField portTextField;
    private final JButton defaultPortButton;
    private final JPasswordField passwordTextField;
    private final JSpinner updateIntervalSpinner;

    public SettingsDialog() {

        /*
         * Begin code managed by Window builder
         */

        contentPane = new JPanel();
        tabbedPane = new JTabbedPane();
        tabbedPane.setToolTipText("");
        final JPanel panel3 = new JPanel();
        panel3.setToolTipText("");
        tabbedPane.addTab("Connection", panel3);
        panel3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null));
        final JLabel label1 = new JLabel();
        label1.setText("Server name / IP:");
        final JLabel label2 = new JLabel();
        label2.setText("Port:");
        serverTextField = new JFormattedTextField();
        final JLabel label3 = new JLabel();
        label3.setText("User:");
        userTextField = new JTextField();
        final JLabel label4 = new JLabel();
        label4.setText("Password:");
        passwordTextField = new JPasswordField();
        portTextField = new JFormattedTextField();
        defaultPortButton = new JButton();
        defaultPortButton.setText("Default");

        GroupLayout gl_panel3 = new GroupLayout(panel3);
        gl_panel3.setHorizontalGroup(gl_panel3.createParallelGroup(Alignment.LEADING).addGroup(
                gl_panel3
                        .createSequentialGroup()
                        .addContainerGap()
                        .addGroup(
                                gl_panel3.createParallelGroup(Alignment.LEADING).addComponent(label1)
                                        .addComponent(label2).addComponent(label3).addComponent(label4))
                        .addPreferredGap(ComponentPlacement.RELATED, 10, GroupLayout.PREFERRED_SIZE)
                        .addGroup(
                                gl_panel3
                                        .createParallelGroup(Alignment.TRAILING)
                                        .addComponent(passwordTextField)
                                        .addComponent(userTextField)
                                        .addGroup(
                                                Alignment.LEADING,
                                                gl_panel3
                                                        .createSequentialGroup()
                                                        .addComponent(portTextField, GroupLayout.PREFERRED_SIZE, 102,
                                                                GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(ComponentPlacement.UNRELATED)
                                                        .addComponent(defaultPortButton))
                                        .addComponent(serverTextField, GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE))
                        .addContainerGap()));

        gl_panel3.setVerticalGroup(gl_panel3.createParallelGroup(Alignment.LEADING).addGroup(
                gl_panel3
                        .createSequentialGroup()
                        .addGap(14)
                        .addGroup(
                                gl_panel3
                                        .createParallelGroup(Alignment.BASELINE)
                                        .addComponent(label1)
                                        .addComponent(serverTextField, GroupLayout.PREFERRED_SIZE,
                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(ComponentPlacement.RELATED)
                        .addGroup(
                                gl_panel3
                                        .createParallelGroup(Alignment.BASELINE)
                                        .addComponent(label2)
                                        .addComponent(portTextField, GroupLayout.PREFERRED_SIZE,
                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(defaultPortButton))
                        .addGap(28)
                        .addGroup(
                                gl_panel3
                                        .createParallelGroup(Alignment.BASELINE)
                                        .addComponent(label3)
                                        .addComponent(userTextField, GroupLayout.PREFERRED_SIZE,
                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(ComponentPlacement.UNRELATED)
                        .addGroup(
                                gl_panel3
                                        .createParallelGroup(Alignment.BASELINE)
                                        .addComponent(label4)
                                        .addComponent(passwordTextField, GroupLayout.PREFERRED_SIZE,
                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(45, Short.MAX_VALUE)));

        panel3.setLayout(gl_panel3);
        panel3.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { serverTextField, portTextField,
                defaultPortButton, userTextField, passwordTextField, label1, label2, label3, label4 }));

        final JPanel panel5 = new JPanel();
        tabbedPane.addTab("Other", panel5);
        panel5.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null));

        final JLabel label5 = new JLabel();
        label5.setText("Update interval:");
        updateIntervalSpinner = new JSpinner();

        GroupLayout gl_panel5 = new GroupLayout(panel5);
        gl_panel5.setHorizontalGroup(gl_panel5.createParallelGroup(Alignment.LEADING).addGroup(
                gl_panel5.createSequentialGroup().addContainerGap().addComponent(label5)
                        .addPreferredGap(ComponentPlacement.UNRELATED)
                        .addComponent(updateIntervalSpinner, GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                        .addContainerGap()));
        gl_panel5.setVerticalGroup(gl_panel5.createParallelGroup(Alignment.LEADING).addGroup(
                gl_panel5
                        .createSequentialGroup()
                        .addGap(5)
                        .addGroup(
                                gl_panel5
                                        .createParallelGroup(Alignment.BASELINE)
                                        .addComponent(label5)
                                        .addComponent(updateIntervalSpinner, GroupLayout.PREFERRED_SIZE,
                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));

        panel5.setLayout(gl_panel5);
        panel5.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { updateIntervalSpinner, label5 }));

        setContentPane(contentPane);
        setModal(true);
        setResizable(false);
        setTitle("Settings");
        buttonCancel = new JButton();
        buttonCancel.setText("Cancel");

        buttonOK = new JButton();
        buttonOK.setText("OK");
        getRootPane().setDefaultButton(buttonOK);

        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                Alignment.TRAILING,
                gl_contentPane
                        .createSequentialGroup()
                        .addContainerGap()
                        .addGroup(
                                gl_contentPane
                                        .createParallelGroup(Alignment.TRAILING)
                                        .addComponent(tabbedPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 424,
                                                Short.MAX_VALUE)
                                        .addGroup(
                                                gl_contentPane.createSequentialGroup().addComponent(buttonOK)
                                                        .addPreferredGap(ComponentPlacement.RELATED)
                                                        .addComponent(buttonCancel))).addContainerGap()));
        gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                Alignment.TRAILING,
                gl_contentPane
                        .createSequentialGroup()
                        .addContainerGap()
                        .addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                        .addPreferredGap(ComponentPlacement.RELATED)
                        .addGroup(
                                gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(buttonCancel)
                                        .addComponent(buttonOK)).addContainerGap()));
        contentPane.setLayout(gl_contentPane);

        /*
         * End code managed by Window builder
         */

        defaultPortButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                portTextField.setText("6800");
            }
        });

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            serverTextField.setText(Config.getInstance().getHost());
            int port = Config.getInstance().getPort();
            if (port == 0) {
                portTextField.setText("");
            } else {
                portTextField.setText(Integer.toString(Config.getInstance().getPort()));
            }
            userTextField.setText(Config.getInstance().getRpcUser());
            passwordTextField.setText(Config.getInstance().getRpcPass());
            updateIntervalSpinner.setValue(Config.getInstance().getUpdateInterval());
        }
        super.setVisible(b);
    }

    private void onOK() {
        try {
            Config.getInstance().setRpcURL(serverTextField.getText(), Integer.parseInt(portTextField.getText()));
            Config.getInstance().setRpcUser(userTextField.getText());
            Config.getInstance().setRpcPass(new String(passwordTextField.getPassword()));
            Config.getInstance().setUpdateInterval(Long.parseLong(updateIntervalSpinner.getValue().toString()));
            Config.getInstance().pushPreferences();

            EventBusFactory.getDefault().fire(new ConfigUpdatedEvent());
            // Arietta.onConfigUpdated();

            dispose();
        } catch (InvalidSettingException e) {
            Arietta.handleException(e);
        }
    }

    private void onCancel() {
        setVisible(false);
    }

}
