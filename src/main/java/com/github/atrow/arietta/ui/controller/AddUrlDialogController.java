package com.github.atrow.arietta.ui.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;

import com.github.atrow.arietta.Config;
import com.github.atrow.arietta.task.TaskManager;
import com.github.atrow.arietta.ui.view.AddUrlDialog;

/**
 * Modal dialog which manages new download URL addition
 * @author AtRow
 *
 */
public class AddUrlDialogController {

    private final AddUrlDialog view;
    
    // FIXME: Hack with auto paste
    private final JTextField urlHelperTextField = new JTextField();
    
    public AddUrlDialogController() {
        view = new AddUrlDialog();        
        view.setController(this);
    }
        
    public void onConfirm() {
        List<String> list = new ArrayList<String>();
        String url = view.getUrlText();
        list.add(url);
        
        TaskManager.getInstance().addUri(list);

        view.setVisible(false);
    }

    public void onCancel() {
    	view.setVisible(false);
    }

    public void show() {
        view.pack();
        view.setLocationRelativeTo(null);
        view.setPasteAutomatically(Config.getInstance().getAddURLAutoPaste());
        pasteUrlFromClipboard();
        view.setVisible(true);
    }

    private void pasteUrlFromClipboard() {
        if (Config.getInstance().getAddURLAutoPaste()) {
            
            String text = getPasteString();
            URL url = getUrlFromString(text);
            if (url != null) {
                view.setUrlText(url.toString());
            }
        }        
    }

    public void onPasteAutomaticallyChange(boolean enabled) {
        Config.getInstance().setAddURLAutoPaste(enabled);
    }

    // FIXME: Hack with clipboard
    public String getPasteString() {
        urlHelperTextField.setText("");
        urlHelperTextField.paste();
        return urlHelperTextField.getText();
    }
    
    private URL getUrlFromString(String text) {
        try {
            return new URL(text);
        } catch (MalformedURLException e) {
            return null;
        }
    }

}
