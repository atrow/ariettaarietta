package com.github.atrow.arietta;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.github.atrow.arietta.download.DownloadStatus;

/**
 * Class used for filtering only downloads with certain state
 */
public class StateFilter {

    private static final List<DownloadStatus> STATUSES_RUNNING = Arrays.asList(
	    DownloadStatus.NEW, 
	    DownloadStatus.ACTIVE
	);

    private static final List<DownloadStatus> STATUSES_STOPPED = Arrays.asList(
	    DownloadStatus.WAITING, 
	    DownloadStatus.PAUSED
	);

    private static final List<DownloadStatus> STATUSES_DONE = Arrays.asList(
	    DownloadStatus.ERROR,
	    DownloadStatus.COMPLETE,
	    DownloadStatus.REMOVED
	);

    private static final List<DownloadStatus> STATUSES_ALL = Arrays.asList(
	    DownloadStatus.UNKNOWN,
	    DownloadStatus.NEW,
	    DownloadStatus.ACTIVE,
	    DownloadStatus.WAITING,
	    DownloadStatus.PAUSED,
	    DownloadStatus.ERROR,
	    DownloadStatus.COMPLETE,
	    DownloadStatus.REMOVED
	);

    /**
     * List of Statuses, which are acceptable for this filter
     */
    private final List<DownloadStatus> currentStatuses;

    /**
     * Constructor
     * 
     * @param statusList
     *            List of Statuses, which are acceptable for this filter
     */
    public StateFilter(List<DownloadStatus> statusList) {
	this.currentStatuses = statusList;
    }

    /**
     * Fabric, that returns new StateFilter with predefined set of Statuses All
     * statuses
     * 
     * @return StateFilter
     */
    public static StateFilter getStateFilterAll() {
	return new StateFilter(STATUSES_ALL);
    }

    /**
     * Fabric, that returns new StateFilter with predefined set of Statuses
     * Statuses typical for Running downloads
     * 
     * @return StateFilter
     */
    public static StateFilter getStateFilterRunning() {
	return new StateFilter(STATUSES_RUNNING);
    }

    /**
     * Fabric, that returns new StateFilter with predefined set of Statuses
     * Statuses typical for Stopped downloads
     * 
     * @return StateFilter
     */
    public static StateFilter getStateFilterStopped() {
	return new StateFilter(STATUSES_STOPPED);
    }

    /**
     * Fabric, that returns new StateFilter with predefined set of Statuses
     * Statuses typical for Complete downloads
     * 
     * @return StateFilter
     */
    public static StateFilter getStateFilterDone() {
	return new StateFilter(STATUSES_DONE);
    }

    /**
     * Matching function
     * 
     * @param download
     *            Download object
     * @return True, if fits current States set
     */
    public boolean contains(DownloadStatus status) {
	return currentStatuses.contains(status);
    }

    /**
     * Does a filter contains any Running statuses
     * 
     * @return true if contains
     */
    public boolean containsRunning() {
	return CollectionUtils.containsAny(currentStatuses, STATUSES_RUNNING);
    }

    /**
     * Does a filter contains any Stopped statuses
     * 
     * @return true if contains
     */
    public boolean containsStopped() {
	return CollectionUtils.containsAny(currentStatuses, STATUSES_STOPPED);
    }

    /**
     * Does a filter contains any Done statuses
     * 
     * @return true if contains
     */
    public boolean containsDone() {
	return CollectionUtils.containsAny(currentStatuses, STATUSES_DONE);
    }

}