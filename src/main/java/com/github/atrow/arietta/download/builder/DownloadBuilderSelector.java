package com.github.atrow.arietta.download.builder;

import java.util.Map;

import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class DownloadBuilderSelector {

    @SuppressWarnings("unchecked")
    public static DownloadBuilder getDownloadBuilder(Object rawDownloadData) throws InvalidRpcDataException {
        
        if (rawDownloadData == null) {
            throw new InvalidRpcDataException("Got null when trying to parse raw Download data");
        }
        
        if (rawDownloadData instanceof String) {
            return new EmptyDownloadBuilder();

        } else if (rawDownloadData instanceof Map) {

            Map<String, Object> rawMap = (Map<String, Object>) rawDownloadData;

            Object[] files = (Object[]) rawMap.get("files");

            boolean isMultiFile = (files != null) && (files.length > 0);
            boolean isTorrent = rawMap.get("bittorrent") != null;
            // boolean hasChildren = rawMap.get("followedBy") != null;
            // boolean hasParent = rawMap.get("belongsTo") != null;

            if (isTorrent) {
                return new TorrentDownloadBuilder();

            } else if (isMultiFile) {
                return new MetalinkDownloadBuilder();

            } else {
                return new BasicDownloadBuilder();

            }

        } else {
            throw new InvalidRpcDataException("Got wrong data structure when trying to parse raw Download data");
        }
        
    }

}
