package com.github.atrow.arietta.download.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.download.DownloadStatus;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.parser.IntegerListParser;
import com.github.atrow.arietta.task.parser.IntegerParser;
import com.github.atrow.arietta.task.parser.LongParser;
import com.github.atrow.arietta.task.parser.StatusParser;
import com.github.atrow.arietta.task.parser.StringParser;

public abstract class AbstractDownloadBuilder implements DownloadBuilder {

    private Object rawDownloadData;

    private Map<String, Object> rawMap;

    private Download download;

    @Override
    public Download getDownload() {
        return download;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void createNewDownload(Object rawDownloadData) throws InvalidRpcDataException {
        this.rawDownloadData = rawDownloadData;

        if (rawDownloadData == null) {
            throw new InvalidRpcDataException("Got null when trying to parse raw Download data");
        }

        if (rawDownloadData instanceof Map<?, ?>) {
            try {
                rawMap = (Map<String, Object>) rawDownloadData;
            } catch (ClassCastException e) {
                throw new InvalidRpcDataException("Got wrong data structure when trying to parse raw Download data", e);
            }
        }

        download = new Download();
    }

    List<Integer> getAsIntList(String key) throws InvalidRpcDataException {

        Object object = rawMap.get(key);
        return IntegerListParser.parseNumericStringArrayObject(object);
    }

    
    String getAsString(String key) throws InvalidRpcDataException {

        Object object = rawMap.get(key);
        return StringParser.parseStringObject(object);
    }

    DownloadStatus getAsStatus(String key) throws InvalidRpcDataException {

        Object object = rawMap.get(key);
        return StatusParser.parseStatusObject(object);
    }

    Integer getAsInteger(String key) throws InvalidRpcDataException {

        Object object = rawMap.get(key);
        return IntegerParser.parseNumericStringObject(object);
    }

    Long getAsLong(String key) throws InvalidRpcDataException {

        Object object = rawMap.get(key);
        return LongParser.parseNumericStringObject(object);
    }

    List<Download.File> getFiles() throws InvalidRpcDataException {

        List<Download.File> list = new ArrayList<Download.File>();

        Object object = rawMap.get("files");

        if (object instanceof Object[]) {
            Object[] objectArray = (Object[]) object;

            for (Object fileObject : objectArray) {
                list.add(new Download.File(fileObject));
            }

        } else {
            throw new InvalidRpcDataException("Got wrong data structure when trying to parse raw Download data");
        }

        return list;
    }

    Object getRawDownloadData() {
        return rawDownloadData;
    }

    Map<String, Object> getRawMap() {
        return rawMap;
    }

}
