package com.github.atrow.arietta.download.builder;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.download.DownloadStatus;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.parser.IntegerParser;

public class EmptyDownloadBuilder extends AbstractDownloadBuilder {

    @Override
    public void buildCommonData() throws InvalidRpcDataException {
        
        Download download = getDownload();

        Object object = getRawDownloadData();

        Integer gid = IntegerParser.parseNumericStringObject(object);

        download.setGid(gid);

        download.setStatus(DownloadStatus.NEW);

        download.setName("Unknown");
        
    }

    @Override
    public void buildMetalinkData() {
        // Do nothing.
    }

    @Override
    public void buildTorrentData() throws InvalidRpcDataException {
        // Do nothing.
    }

}
