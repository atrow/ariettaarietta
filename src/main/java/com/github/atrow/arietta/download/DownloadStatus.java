package com.github.atrow.arietta.download;

/**
 * List of statuses, in which a download can be
 */
public enum DownloadStatus {
    UNKNOWN, 
    NEW, 
    ACTIVE,
    WAITING, 
    PAUSED, 
    ERROR, 
    COMPLETE, 
    REMOVED
}