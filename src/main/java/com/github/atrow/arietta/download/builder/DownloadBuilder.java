package com.github.atrow.arietta.download.builder;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.exception.InvalidRpcDataException;

public interface DownloadBuilder {

    Download getDownload();

    void createNewDownload(Object rawDownloadData) throws InvalidRpcDataException;

    void buildCommonData() throws InvalidRpcDataException;

    void buildMetalinkData();

    void buildTorrentData() throws InvalidRpcDataException;

}
