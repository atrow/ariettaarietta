package com.github.atrow.arietta.download.builder;

import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class BasicDownloadBuilder extends AbstractDownloadBuilder {

    @Override
    public void buildCommonData() throws InvalidRpcDataException {
        
        Download download = getDownload();

        download.setGid(getAsInteger("gid"));

        download.setName(getNameFromFilePath());

        download.setStatus(getAsStatus("status"));

        download.setTotalLength(getAsLong("totalLength"));

        download.setCompletedLength(getAsLong("completedLength"));

        download.setUploadLength(getAsLong("uploadLength"));

        download.setBitfield(getAsString("bitfield"));

        download.setDownloadSpeed(getAsLong("downloadSpeed"));

        download.setUploadSpeed(getAsLong("uploadSpeed"));

        download.setPieceLength(getAsLong("pieceLength"));

        download.setNumPieces(getAsLong("numPieces"));

        download.setConnections(getAsLong("connections"));

        download.setErrorCode(getAsString("errorCode"));

        download.setFollowedBy(getAsIntList("followedBy"));

        download.setBelongsTo(getAsInteger("belongsTo"));

        download.setDir(getAsString("dir"));
        
    }

    private String getNameFromFilePath() throws InvalidRpcDataException {
   
        List<Download.File> files = getFiles();

        if (!files.isEmpty()) {
            int index = files.get(0).path.lastIndexOf("/");
            return files.get(0).path.substring(index + 1);

        } else {
            return null;
        }
        
    }

    @Override
    public void buildMetalinkData() {
        // Do nothing.
    }

    @Override
    public void buildTorrentData() throws InvalidRpcDataException {
        // Do nothing.
    }

}
