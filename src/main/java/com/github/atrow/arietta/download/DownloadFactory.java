package com.github.atrow.arietta.download;

import com.github.atrow.arietta.download.builder.DownloadBuilder;
import com.github.atrow.arietta.download.builder.DownloadBuilderSelector;
import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class DownloadFactory {

    public static Download createNewDownload(Object rawDownloadData) throws InvalidRpcDataException {

        DownloadBuilder builder = DownloadBuilderSelector.getDownloadBuilder(rawDownloadData);

        builder.createNewDownload(rawDownloadData);
        builder.buildCommonData();
        builder.buildMetalinkData();
        builder.buildTorrentData();

        return builder.getDownload();
    }

}
