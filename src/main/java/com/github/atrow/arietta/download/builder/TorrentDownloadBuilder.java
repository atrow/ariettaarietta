package com.github.atrow.arietta.download.builder;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class TorrentDownloadBuilder extends BasicDownloadBuilder {

    @Override
    public void buildTorrentData() throws InvalidRpcDataException {

        Download download = getDownload();

        download.setInfoHash(getAsInteger("infoHash"));

        download.setNumSeeders(getAsLong("numSeeders"));

    }

}
