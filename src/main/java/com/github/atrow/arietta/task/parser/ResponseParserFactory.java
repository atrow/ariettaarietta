package com.github.atrow.arietta.task.parser;


/**
 *
 */
@Deprecated
public final class ResponseParserFactory {

    private static ResponseParserFactory instance;

    private final DownloadListParser downloadListParser = new DownloadListParser();
    private final DownloadParser downloadParser = new DownloadParser();
    private final OptionsParser optionsParser = new OptionsParser();
    private final IntegerListParser gidListParser = new IntegerListParser();
    private final StringParser stringParser = new StringParser();
    private final IntegerParser gidParser = new IntegerParser();

    public static synchronized ResponseParserFactory getInstance() {
        if (instance == null) {
            instance = new ResponseParserFactory();
        }
        return instance;
    }

    public DownloadListParser getDownloadListParser() {
        return downloadListParser;
    }

    public DownloadParser getDownloadParser() {
        return downloadParser;
    }

    public OptionsParser getOptionsParser() {
        return optionsParser;
    }

    public IntegerListParser getGidListParser() {
        return gidListParser;
    }

    public StringParser getStringParser() {
        return stringParser;
    }

    public IntegerParser getGidParser() {
        return gidParser;
    }

}
