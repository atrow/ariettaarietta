package com.github.atrow.arietta.task.impl;

import java.util.List;

import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.RemoteDownloadsChangedEvent;

public class RemoveTask extends AbstractTask {

    public RemoveTask(List<Object> params) {
        super(Task.METHOD_REMOVE, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {
        
        /*
         * TODO: fire single-updating event foe concrete gid
         * Integer downloadGid = getGidResult();
         */
        EventBusFactory.getDefault().fire(new RemoteDownloadsChangedEvent());
    }

}
