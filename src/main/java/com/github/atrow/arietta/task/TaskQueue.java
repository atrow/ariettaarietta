package com.github.atrow.arietta.task;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import com.github.atrow.arietta.Logger;
import com.github.atrow.arietta.event.EventBus;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.event.impl.ConnectionLostEvent;
import com.github.atrow.arietta.event.impl.QueueFullEvent;
import com.github.atrow.arietta.event.impl.QueueReadyEvent;

/**
 * RPC class. Establishes connection to server. Runs calls, handles responses.
 */
public final class TaskQueue {

    /**
     * Maximum queue size. It shouldn't be large
     */
    private static final int MAXQSIZE = 5;

    /**
     * Stores all pending tasks
     */
    private final BlockingQueue<Task> queue;

    /**
     * 
     */
    private boolean ready = true;

    /**
     * Singleton instance
     */
    private static TaskQueue instance;

    /**
     * Singleton constructor
     */
    private TaskQueue() {
        queue = new PriorityBlockingQueue<Task>(MAXQSIZE);

        // TODO: Move Events out of Queue
        setupEventListeners();
    }

    private void setupEventListeners() {
        EventBus bus = EventBusFactory.getDefault();

        bus.registerEventListener(new ConnectionLostEvent.Listener() {

            @Override
            public void handle(ConnectionLostEvent event) {
                purgeQueue();
            }
        });
    }

    /**
     * @return Single TaskQueue instance
     */
    public static synchronized TaskQueue getInstance() {
        if (instance == null) {
            instance = new TaskQueue();
        }
        return instance;
    }

    /**
     * Adds task to queue. Controls queue overflow.
     * 
     * @param task
     *            instance of Task to be added to queue.
     * @throws QueueFullException
     *             when MAXQSIZE reached
     */
    public void addTask(Task task) {

        if (queue.size() <= MAXQSIZE) {

            Logger.getInstance().debug("QSize is: " + queue.size() + "; Now Pushed task " + task.toString());
            queue.add(task);
            Logger.getInstance().debug("QSize is: " + queue.size());

        } else {
            Logger.getInstance().info("QSize is FULL: " + queue.size() + "; Task " + task.toString() + " Ignored");
        }

        if (isBecomeFull()) {
            Logger.getInstance().info("Queue became full on Task " + task.toString());
            
            //Signal Controller that queue is full.
            EventBusFactory.getDefault().fire(new QueueFullEvent());
        }
    }

    /**
     * @return Next available task from Queue
     * @throws InterruptedException
     *             when interrupted waiting for appearance of the first element
     *             in queue
     */
    public Task getTaskOrWait() throws InterruptedException {
        // Wait while next task appears in queue
        Task task = queue.take();
        if (isBecomeReady()) {
            EventBusFactory.getDefault().fire(new QueueReadyEvent());
        }
        return task;
    }
    
    /**
     * Ensures that queue is ready to accept MAXBGTASKS+1 new tasks (all
     * background plus one foreground)
     * 
     * @return true if there is enough free space in queue, otherwise - false
     */
    private boolean isReady() {
        return (queue.size() <= MAXQSIZE);
    }
    
    /**
     * @return true if queue was ready, but become full since last check
     */
    private boolean isBecomeFull() {
        // Was ready, but become not
        boolean wasReady = ready;
        ready = isReady();
        return (wasReady && !ready);
    }
    
    /**
     * @return true if queue was full, but become ready since last check
     */
    private boolean isBecomeReady() {
        boolean wasReady = ready;
        ready = isReady();
        return (!wasReady && ready);
    }
    
    /**
     * Empty tasks queue
     */
    private void purgeQueue() {
        queue.clear();
    }

}
