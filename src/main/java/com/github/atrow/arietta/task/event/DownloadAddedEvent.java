/**
 * 
 */
package com.github.atrow.arietta.task.event;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class DownloadAddedEvent implements Event {
    
    private final Download download;
    
    public DownloadAddedEvent(Download download) {
        this.download = download;
    }
    
    public Download getDownload() {
        return download;
    }

    public static abstract class Listener extends AbstractEventListener<DownloadAddedEvent> {

        @Override
        public abstract void handle(DownloadAddedEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof DownloadAddedEvent);
        }
        
    }
    
}