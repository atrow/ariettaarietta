package com.github.atrow.arietta.task.impl;

import java.util.List;

import com.github.atrow.arietta.Arietta;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.DownloadRemovedEvent;

public class RemoveDownloadResultTask extends AbstractTask {

    private Integer downloadGid;
    
    public RemoveDownloadResultTask(List<Object> params) {
        super(Task.METHOD_REMOVE_DOWNLOAD_RESULT, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {
        
        String result = getStringResult();
        if (result.equalsIgnoreCase(STATUS_STRING_OK)) {
            EventBusFactory.getDefault().fire(new DownloadRemovedEvent(downloadGid));
        } else {
            // TODO: handle
            Arietta.handleException(new Exception("Got unexpected result"));
        }
    }

    public void setDownloadGid(int gid) {
        this.downloadGid = gid;
    }

}
