package com.github.atrow.arietta.task.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class IntegerListParser {

    public static List<Integer> parseNumericStringArrayObject(Object object) throws InvalidRpcDataException {

        if (object == null) {
            return null;
        }

        if (object instanceof String[]) {
            return parseNumericStringArray((String[]) object);

        } else {
            throw new InvalidRpcDataException("Expected an array of numeric Strings, but got: "
                    + object.getClass().getName());
        }
    }

    public static List<Integer> parseNumericStringArray(String[] strings) throws InvalidRpcDataException {
        try {

            if (strings == null) {
                return null;
            }

            List<Integer> list = new ArrayList<Integer>();

            for (String string : strings) {
                list.add(Integer.parseInt(string));
            }
            return list;

        } catch (NumberFormatException e) {
            throw new InvalidRpcDataException("Expected an array of numeric Strings, but got: "
                    + Arrays.toString(strings));
        }
    }

}