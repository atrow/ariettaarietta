package com.github.atrow.arietta.task;

import com.github.atrow.arietta.exception.InvalidRpcDataException;

public interface Task extends Comparable<Task> {

    String METHOD_ADD_METALINK = "aria2.addMetalink";
    String METHOD_ADD_TORRENT = "aria2.addTorrent";
    String METHOD_ADD_URI = "aria2.addUri";
    String METHOD_FORCE_REMOVE = "aria2.forceRemove";
    String METHOD_GET_OPTION = "aria2.getOption";
    String METHOD_GET_VERSION = "getVersion";
    String METHOD_PAUSE = "aria2.pause";
    String METHOD_PAUSE_ALL = "aria2.pause";
    String METHOD_PURGE_DOWNLOAD_RESULT = "aria2.purgeDownloadResult";
    String METHOD_REMOVE = "aria2.remove";
    String METHOD_REMOVE_DOWNLOAD_RESULT = "aria2.removeDownloadResult";
    String METHOD_TELL_ACTIVE = "aria2.tellActive";
    String METHOD_TELL_STOPPED = "aria2.tellStopped";
    String METHOD_TELL_WAITING = "aria2.tellWaiting";
    String METHOD_UNPAUSE = "aria2.unpause";
    String METHOD_UNPAUSE_ALL = "aria2.unpauseAll";

    String STATUS_STRING_OK = "ok";
    
    /**
     * Task can be foreground and background. In fact, only difference between
     * them is it's priority in a queue. Foreground tasks always go ahead of the
     * background. They are executed sequentially in one thread.
     * 
     */
    enum Priority {
        FOREGROUND, BACKGROUND;
    }


    
    AbstractTask.Priority getTaskPriority();

    void handleResult() throws InvalidRpcDataException;

    void execute();

}