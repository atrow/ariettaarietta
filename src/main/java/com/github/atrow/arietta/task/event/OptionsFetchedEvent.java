/**
 * 
 */
package com.github.atrow.arietta.task.event;

import com.github.atrow.arietta.Options;
import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class OptionsFetchedEvent implements Event {

    private Options options;
    
    public OptionsFetchedEvent(Options options) {
        this.options = options;
    }

    Options getOptions() {
        return options;
    }
  
    public static abstract class Listener extends AbstractEventListener<OptionsFetchedEvent> {

        @Override
        public abstract void handle(OptionsFetchedEvent event);

        @Override
        public boolean canHandle(Event event) {
            return (event instanceof OptionsFetchedEvent);
        }
        
    }
    
}
