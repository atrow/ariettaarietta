package com.github.atrow.arietta.task.parser;

import com.github.atrow.arietta.download.DownloadStatus;
import com.github.atrow.arietta.exception.InvalidRpcDataException;


public class StatusParser {

    public static DownloadStatus parseStatusObject(Object object) throws InvalidRpcDataException {

        String stringStatus = StringParser.parseStringObject(object);

        try {
            return DownloadStatus.valueOf(stringStatus.toUpperCase());
        } catch (IllegalArgumentException e) {
            return DownloadStatus.UNKNOWN;
        }
    }
}

