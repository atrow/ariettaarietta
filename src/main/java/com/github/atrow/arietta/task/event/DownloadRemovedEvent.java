/**
 * 
 */
package com.github.atrow.arietta.task.event;

import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class DownloadRemovedEvent implements Event {
    
    private Integer downloadGid;
    
    public DownloadRemovedEvent(Integer gid) {
        this.downloadGid = gid;
    }
    
    public Integer getDownloadGid() {
        return downloadGid;
    }

    public static abstract class Listener extends AbstractEventListener<DownloadRemovedEvent> {

        @Override
        public abstract void handle(DownloadRemovedEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof DownloadRemovedEvent);
        }
        
    }
    
}