package com.github.atrow.arietta.task.parser;

import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class LongParser {

    public static Long parseNumericStringObject(Object object) throws InvalidRpcDataException {

        if (object == null) {
            return null;
        }

        if (object instanceof String) {
            return parseNumericString((String) object);

        } else {
            throw new InvalidRpcDataException("Expected numeric String, but got: " + object.getClass().getName());
        }
    }

    public static Long parseNumericString(String string) throws InvalidRpcDataException {
        try {
            return Long.parseLong(string);
        } catch (NumberFormatException e) {

            if (string == null) {
                throw new InvalidRpcDataException("Expected numeric String, but got null");
            } else {
                throw new InvalidRpcDataException("Expected numeric String, but got: " + string);
            }
        }
    }

}