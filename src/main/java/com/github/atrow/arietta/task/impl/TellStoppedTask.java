package com.github.atrow.arietta.task.impl;

import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.DownloadsFetchedEvent;

public class TellStoppedTask extends AbstractTask {

    public TellStoppedTask(List<Object> params) {
        super(Task.METHOD_TELL_STOPPED, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {
        
        List<Download> downloads = getDownloadListResult();
        EventBusFactory.getDefault().fire(new DownloadsFetchedEvent(downloads));
    }

}