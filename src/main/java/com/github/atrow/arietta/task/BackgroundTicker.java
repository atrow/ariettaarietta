package com.github.atrow.arietta.task;

import java.util.Date;

import com.github.atrow.arietta.Config;
import com.github.atrow.arietta.Logger;
import com.github.atrow.arietta.StateFilter;
import com.github.atrow.arietta.event.EventBus;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.event.impl.ConfigUpdatedEvent;
import com.github.atrow.arietta.event.impl.ConnectionEstablishedEvent;
import com.github.atrow.arietta.event.impl.ConnectionLostEvent;
import com.github.atrow.arietta.event.impl.StateFilterChangedEvent;
import com.github.atrow.arietta.task.event.DownloadRemovedEvent;
import com.github.atrow.arietta.task.event.RemoteDownloadsChangedEvent;

/**
 * Separate Runnable to run periodic background tasks in separate Thread
 */
public class BackgroundTicker {
    
    private final TickerRunnable tickerRunnable;
    
    private final Thread tickerThread;

    protected StateFilter stateFilter;
    
    private static BackgroundTicker instance;
    
    private BackgroundTicker() {
        setupEventListeners();
        
        tickerRunnable = new TickerRunnable();
        tickerRunnable.setUpdateInterval(Config.getInstance().getUpdateInterval() * 1000);
        
        tickerThread = new Thread(tickerRunnable, "Ticker");
    }

    public static synchronized BackgroundTicker getInstance() {
        if (instance == null) {
            instance = new BackgroundTicker();
        }
        return instance;
    }
    
    
    private void setupEventListeners() {

        EventBus eventBus = EventBusFactory.getDefault();

        eventBus.registerEventListener(new ConfigUpdatedEvent.Listener() {
            @Override
            public void handle(ConfigUpdatedEvent event) {
                interrupt();
            }
        });

        eventBus.registerEventListener(new ConnectionEstablishedEvent.Listener() {
            @Override
            public void handle(ConnectionEstablishedEvent event) {
                start();
            }
        });

        eventBus.registerEventListener(new ConnectionLostEvent.Listener() {
            @Override
            public void handle(ConnectionLostEvent event) {
                stop();
            }
        });
        
        eventBus.registerEventListener(new DownloadRemovedEvent.Listener() {
            @Override
            public void handle(DownloadRemovedEvent event) {
                doUpdateNow();
            }
        });

        eventBus.registerEventListener(new RemoteDownloadsChangedEvent.Listener() {
            @Override
            public void handle(RemoteDownloadsChangedEvent event) {
                doUpdateNow();
            }
        });

        eventBus.registerEventListener(new StateFilterChangedEvent.Listener() {
            @Override
            public void handle(StateFilterChangedEvent event) {
                stateFilter = event.getStateFilter();
            }
        });

    }
    
    private void start() {
        tickerRunnable.setUpdateInterval(Config.getInstance().getUpdateInterval() * 1000);
        
        if (!tickerThread.isAlive()) {
            tickerThread.start();
        }
    }
    
    private void stop() {
        tickerRunnable.setUpdateInterval(Long.MAX_VALUE);
    }
    
    private void interrupt() {
        tickerRunnable.setUpdateInterval(Config.getInstance().getUpdateInterval() * 1000);
        tickerThread.interrupt();
    }
    
    private void doUpdateNow() {
        tickerRunnable.doUpdate();
    }
    

    private class TickerRunnable implements Runnable{
        
        private Long updateInterval;
        
        private Long lastUpdateTime = 0L;
        
        private final TaskManager manager = TaskManager.getInstance();
        
        /**
         * @see Thread#run()
         */
        @Override
        public void run() {
            // Execution will be infinite
            while (true) {
                try {

                    while(getTimeToNextUpdate() > 0) {
                        Thread.sleep(getTimeToNextUpdate());
                    }
                    
                    Logger.getInstance().info("Tick");
                    doUpdate();

                } catch (InterruptedException e) {
                    // Do nothing. Just continue loop.
                }
            }
        }

        public void setUpdateInterval(Long millis) {
            this.updateInterval = millis;
        }
        
        private long getTimeToNextUpdate() {
            
            long now = new Date().getTime();
            long timeToNextUpdate = lastUpdateTime + updateInterval - now;
            
            return (timeToNextUpdate > 0) ? timeToNextUpdate : 0;
        }

        public void doUpdate() {
            lastUpdateTime = new Date().getTime();
            
            if (stateFilter != null) {
                if (stateFilter.containsRunning()) {
                    manager.tellActive();
                }
                if (stateFilter.containsStopped()) {
                    manager.tellWaiting();
                }
                if (stateFilter.containsDone()) {
                    manager.tellStopped();
                }

            } else {
                manager.tellActive();
                manager.tellWaiting();
                manager.tellStopped();
            }
        }
    }
    
}
