package com.github.atrow.arietta.task.impl;

import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.VersionFetchedEvent;

public class GetVersionTask extends AbstractTask {

    public GetVersionTask() {
        super(Task.METHOD_GET_VERSION);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {
        
        String ver = getStringResult();
        EventBusFactory.getDefault().fire(new VersionFetchedEvent(ver));            
    }

}
