package com.github.atrow.arietta.task.parser;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.download.DownloadFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class DownloadParser {

    public static Download parseDownloadObject(Object result) throws InvalidRpcDataException {
        return DownloadFactory.createNewDownload(result);
    }
}