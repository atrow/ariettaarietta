package com.github.atrow.arietta.task.impl;

import com.github.atrow.arietta.Arietta;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.RemoteDownloadsChangedEvent;

public class PurgeDownloadResultTask extends AbstractTask {

    public PurgeDownloadResultTask() {
        super(Task.METHOD_PURGE_DOWNLOAD_RESULT);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {

        String result = getStringResult();
        if (result.equalsIgnoreCase(STATUS_STRING_OK)) {
            EventBusFactory.getDefault().fire(new RemoteDownloadsChangedEvent());
        } else {
            // TODO: handle
            Arietta.handleException(new Exception("Got unexpected result"));
        }
    }

}
