package com.github.atrow.arietta.task;

import java.util.ArrayList;
import java.util.List;

import com.github.atrow.arietta.Arietta;
import com.github.atrow.arietta.Options;
import com.github.atrow.arietta.task.impl.AddMetalinkTask;
import com.github.atrow.arietta.task.impl.AddTorrentTask;
import com.github.atrow.arietta.task.impl.AddUriTask;
import com.github.atrow.arietta.task.impl.ForceRemoveTask;
import com.github.atrow.arietta.task.impl.GetOptionsTask;
import com.github.atrow.arietta.task.impl.GetVersionTask;
import com.github.atrow.arietta.task.impl.PauseAllTask;
import com.github.atrow.arietta.task.impl.PauseTask;
import com.github.atrow.arietta.task.impl.PurgeDownloadResultTask;
import com.github.atrow.arietta.task.impl.RemoveDownloadResultTask;
import com.github.atrow.arietta.task.impl.RemoveTask;
import com.github.atrow.arietta.task.impl.TellActiveTask;
import com.github.atrow.arietta.task.impl.TellStatusTask;
import com.github.atrow.arietta.task.impl.TellStoppedTask;
import com.github.atrow.arietta.task.impl.TellWaitingTask;
import com.github.atrow.arietta.task.impl.UnPauseAllTask;
import com.github.atrow.arietta.task.impl.UnPauseTask;

public final class TaskManager {
    
    private static TaskManager instance;
    
    private TaskManager() {
        // Empty
    }

    public static synchronized TaskManager getInstance() {
        if (instance == null) {
            instance = new TaskManager();
        }
        return instance;
    }
    
    /**
     * ARIA2 DESCRIPTION:
     * ---
     * This method adds new HTTP(S)/FTP/BitTorrent Magnet URI. uris is of type array and its element is URI which is
     * of type string. For BitTorrent Magnet URI, uris must have only one element and it should be BitTorrent Magnet URI.
     * URIs in uris must point to the same file. If you mix other URIs which point to another file, aria2 does not
     * complain but download may fail. options is of type struct and its members are a pair of option name and value.
     * See Options below for more details. If position is given as an integer starting from 0, the new download is
     * inserted at position in the waiting queue. If position is not given or position is larger than the size of
     * the queue, it is appended at the end of the queue. This method returns GID of registered download.
     *
     * aria2.addUri method always returns download GID, so we create Download object on every added URL.
     * ---
     *
     * Performs aria2.addUri call with passed parameters. Result returns by callback method.
     * @param callMe Implementation of RPC.Callback, specifies method to be called when comes RPC response
     * @see Callback
     * @param uris List<String> of URIs which points on download target
     * @param options Options for this download
     * @param position int number of position for this download
     *  
     */
    public void addUri(List<String> uris, Options options, int position) {

        if (uris.isEmpty()) {
            //TODO: Need normal exception here
            throw new RuntimeException("URL not specified");
        }
        
        List<Object> list = new ArrayList<Object>();
        list.add(uris);
        list.add(options.serialize());
        list.add(position);

        AddUriTask task = new AddUriTask(list);
        task.setDownloadName(uris.get(0));
        addTask(task);
    }

    public void addUri(List<String> uris) {
        addUri(uris, Arietta.getDefaultOptions(), 0);
    }

    /*
     * This method adds BitTorrent download by uploading .torrent file. If you want to add BitTorrent Magnet URI,
     * use aria2.addUri method instead. torrent is of type base64 which contains Base64-encoded .torrent file.
     * uris is of type array and its element is URI which is of type string. uris is used for Web-seeding.
     * For single file torrents, URI can be a complete URI pointing to the resource or if URI ends with /, name in
     * torrent file is added. For multi-file torrents, name and path in torrent are added to form a URI for each file.
     * Options is of type struct and its members are a pair of option name and value. See Options below for more details.
     * If position is given as an integer starting from 0, the new download is inserted at position in the waiting queue.
     * If position is not given or position is larger than the size of the queue, it is appended at the end of the queue.
     * This method returns GID of registered download. The uploaded data is saved as a file named hex string
     * of SHA-1 hash of data plus ".torrent" in the directory specified by --dir option.
     * The example of filename is 0a3893293e27ac0490424c06de4d09242215f0a6.torrent.
     * If same file already exists, it is overwritten.
     * If the file cannot be saved successfully, the downloads added by this method are not saved by --save-session.
     */
    public void addTorrent(byte[] torrent, List<String> uris, Options options, int position) {

        List<Object> list = new ArrayList<Object>();
        list.add(torrent);
        list.add(uris);
        list.add(options.serialize());
        list.add(position);

        AddTorrentTask task = new AddTorrentTask(list);
        addTask(task);
    }

    public void addTorrent(byte[] torrent, List<String> uris, Options options) {
        addTorrent(torrent, uris, options, 0);
    }

    public void addTorrent(byte[] torrent, List<String> uris) {
        addTorrent(torrent, uris, Arietta.getDefaultOptions());
    }

    public void addTorrent(byte[] torrent, Options options) {
        addTorrent(torrent, new ArrayList<String>(), options);
    }

    public void addTorrent(byte[] torrent) {
        addTorrent(torrent, Arietta.getDefaultOptions());
    }

    /*
     * This method adds Metalink download by uploading .metalink file. metalink is of type base64
     * which contains Base64-encoded .metalink file. options is of type struct and its members are a pair of option name and value.
     * See Options below for more details. If position is given as an integer starting from 0, the new download
     * is inserted at position in the waiting queue. If position is not given or position is larger than the size
     * of the queue, it is appended at the end of the queue. This method returns array of GID of registered download.
     * The uploaded data is saved as a file named hex string of SHA-1 hash of data plus ".metalink" in the directory
     * specified by --dir option. The example of filename is 0a3893293e27ac0490424c06de4d09242215f0a6.metalink.
     * If same file already exists, it is overwritten. If the file cannot be saved successfully, the downloads added by
     * this method are not saved by --save-session.
     */
    public void addMetalink(byte[] metalink, Options options, int position) {

        List<Object> list = new ArrayList<Object>();
        list.add(metalink);
        list.add(options.serialize());
        list.add(position);
        
        AddMetalinkTask task = new AddMetalinkTask(list);
        addTask(task);
    }

    public void addMetalink(byte[] metalink, Options options) {
        addMetalink(metalink, options, 0);
    }

    public void addMetalink(byte[] metalink) {
        addMetalink(metalink, Arietta.getDefaultOptions());
    }

    /*
     * This method removes the download denoted by gid. gid is of type string. If specified download is in progress,
     * it is stopped at first. The status of removed download becomes "removed". This method returns GID of removed download.
     */
    public void remove(int gid) {
        List<Object> list = new ArrayList<Object>();
        list.add( String.valueOf(gid) );

        RemoveTask task = new RemoveTask(list);
        addTask(task);
    }

    /*
     * This method removes the download denoted by gid. This method behaves just like aria2.remove
     * except that this method removes download without any action which takes time such as contacting BitTorrent tracker.
     */
    public void forceRemove(int gid) {
        List<Object> list = new ArrayList<Object>();
        list.add( String.valueOf(gid) );

        ForceRemoveTask task = new ForceRemoveTask(list);        
        addTask(task);
    }

    /*
     * This method pauses the download denoted by gid. gid is of type string. The status of paused download becomes "paused".
     * If the download is active, the download is placed on the first position of waiting queue.
     * As long as the status is "paused", the download is not started.
     * To change status to "waiting", use aria2.unpause method. This method returns GID of paused download.
     */
    public void pause(int gid) {
        List<Object> list = new ArrayList<Object>();
        list.add( String.valueOf(gid) );

        PauseTask task = new PauseTask(list);
        addTask(task);
    }

    /*
     * This method is equal to calling aria2.forcePause for every active/waiting download. This methods returns "OK"
     * for success.
     */
    public void pauseAll() {
        
        PauseAllTask task = new PauseAllTask();
        addTask(task);
    }

    /*
     * This method changes the status of the download denoted by gid from "paused" to "waiting".
     * This makes the download eligible to restart. gid is of type string. This method returns GID of unpaused download.
     */
    public void unpause(int gid) {
        List<Object> list = new ArrayList<Object>();
        list.add( String.valueOf(gid) );

        UnPauseTask task = new UnPauseTask(list);
        addTask(task);
    }

    /*
     * This method is equal to calling aria2.unpause for every active/waiting download. This methods returns "OK" for success.
     */
    public void unpauseAll() {
        UnPauseAllTask task = new UnPauseAllTask();
        addTask(task);
    }

    /*
     * This method returns download progress of the download denoted by gid. gid is of type string. keys is array of string.
     * If it is specified, the response contains only keys in keys array. If keys is empty or not specified, the
     * response contains all keys. This is useful when you just want specific keys and avoid unnecessary transfers.
     * For example, aria2.tellStatus("1", ["gid", "status"]) returns gid and status key. The response is of type struct
     * and it contains following keys. The value type is string.
     */
    public void tellStatus (int gid, List<String> keys) {
        List<Object> list = new ArrayList<Object>();
        list.add(keys);

        TellStatusTask task = new TellStatusTask(list);
        addTask(task);
    }

    public void tellStatus (int gid) {
        tellStatus(gid, new ArrayList<String>());
    }

    /*
     * This method returns URIs used in the download denoted by gid. gid is of type string. The response is of type
     * array and its element is of type struct and it contains following keys. The value type is string.
     */
    public void getUris(int gid) {
        throw new RuntimeException("Method is not implemented!");
    }

    /*
     * This method returns file list of the download denoted by gid. gid is of type string. The response is of type
     * array and its element is of type struct and it contains following keys. The value type is string.
     */
    public void getFiles(int gid) {
        // And here - the File object
        throw new RuntimeException("Method is not implemented!");
    }

    /*
     * This method returns peer list of the download denoted by gid. gid is of type string.
     * This method is for BitTorrent only.
     * The response is of type array and its element is of type struct and it contains following keys.
     * The value type is string.
     */
    public void getPeers(int gid) {
        //TODO: Peer class
        throw new RuntimeException("Method is not implemented!");
    }

    /*
     * This method returns currently connected HTTP(S)/FTP servers of the download denoted by gid. gid is of type string.
     * The response is of type array and its element is of type struct and it contains following keys. The value type is string.
     */
    public void getServers(int gid) {
        throw new RuntimeException("Method is not implemented!");
    }

    /*
     * This method returns the list of active downloads. The response is of type array and its element is the same struct
     * returned by aria2.tellStatus method. For keys parameter, please refer to aria2.tellStatus method.
     */
    public void tellActive(List<String> keys) {
        List<Object> list = new ArrayList<Object>();
        list.add(keys);

        TellActiveTask task = new TellActiveTask(list);
        addTask(task);
        //DlStateArrayParser
    }

    public void tellActive() {
        tellActive(new ArrayList<String>());
    }

    /*
     * This method returns the list of waiting download, including paused downloads. offset is of type integer and specifies
     * the offset from the download waiting at the front. num is of type integer and specifies the number of
     * downloads to be returned. For keys parameter, please refer to aria2.tellStatus method.
     *
     * If offset is a positive integer, this method returns downloads in the range of [offset, offset+num).
     *
     * offset can be a negative integer. offset == -1 points last download in the waiting queue and
     * offset == -2 points the download before the last download, and so on. The downloads in the response are in reversed order.
     *
     * For example, imagine that three downloads "A","B" and "C" are waiting in this order. aria2.tellWaiting(0, 1) returns ["A"].
     * aria2.tellWaiting(1, 2) returns ["B", "C"]. aria2.tellWaiting(-1, 2) returns ["C", "B"].
     *
     * The response is of type array and its element is the same struct returned by aria2.tellStatus method.
     */
    public void tellWaiting(int offset, int num, List<String> keys) {
        List<Object> list = new ArrayList<Object>();
        list.add(offset);
        list.add(num);
        list.add(keys);

        TellWaitingTask task = new TellWaitingTask(list);
        addTask(task);
        //DlStateArrayParser()
    }

    public void tellWaiting(int offset, int num) {
        tellWaiting(offset, num, new ArrayList<String>());
    }
    
    public void tellWaiting() {
        int num = 1000;
        int offset = 0;

        tellWaiting(offset, num);
    }

    /*
     * This method returns the list of stopped download. offset is of type integer and specifies the offset from the oldest
     * download. num is of type integer and specifies the number of downloads to be returned. For keys parameter, please
     * refer to aria2.tellStatus method.
     *
     * offset and num have the same semantics as aria2.tellWaiting method.
     *
     * The response is of type array and its element is the same struct returned by aria2.tellStatus method.
     */
    public void tellStopped (int offset, int num, List<String> keys) {
        List<Object> list = new ArrayList<Object>();
        list.add(offset);
        list.add(num);
        list.add(keys);

        TellStoppedTask task = new TellStoppedTask(list);
        addTask(task);
        //DlStateArrayParser
    }

    public void tellStopped (int offset, int num) {
        tellStopped(offset, num, new ArrayList<String>());
    }
    
    public void tellStopped() {
        int num = 1000;
        int offset = 0;

        tellStopped(offset, num);
    }

    /*
     * This method changes the position of the download denoted by gid. pos is of type integer. how is of type string.
     * If how is "POS_SET", it moves the download to a position relative to the beginning of the queue.
     * If how is "POS_CUR", it moves the download to a position relative to the current position.
     * If how is "POS_END", it moves the download to a position relative to the end of the queue.
     * If the destination position is less than 0 or beyond the end of the queue, it moves the download to the
     * beginning or the end of the queue respectively. The response is of type integer and it is the destination position.
     *
     * For example, if GID#1 is placed in position 3, aria2.changePosition(1, -1, POS_CUR) will change its position to 2.
     * Additional aria2.changePosition(1, 0, POS_SET) will change its position to 0(the beginning of the queue).
     */
    public void changePosition (int gid, int pos, String how) {
        throw new RuntimeException("Method is not implemented!");
    }
    // X3:
    private static final String POS_SET = "POS_SET";
    private static final String POS_CUR = "POS_CUR";
    private static final String POS_END = "POS_END";

    /*
     * This method removes URIs in delUris from and appends URIs in addUris to download denoted by gid.
     * delUris and addUris are list of string. A download can contain multiple files and URIs are attached to each file.
     * fileIndex is used to select which file to remove/attach given URIs. fileIndex is 1-based. position is used to
     * specify where URIs are inserted in the existing waiting URI list. position is 0-based. When position is omitted,
     * URIs are appended to the back of the list. This method first push removal and then addition.
     * Position is the position after URIs are removed, not the position when this method is called.
     * When removing URI, if same URIs exist in download, only one of them is removed for each URI in delUris.
     * In other words, there are three URIs "http://example.org/aria2" and you want remove them all, you have to
     * specify (at least) 3 "http://example.org/aria2" in delUris. This method returns a list which contains 2 integers.
     * The first integer is the number of URIs deleted. The second integer is the number of URIs added.
     */
    public void changeUri (int gid, int fileIndex, List<String> delUris, List<String> addUris, int position)
            {
        throw new RuntimeException("Method is not implemented!");
    }

    public void changeUri (int gid, int fileIndex, List<String> delUris, List<String> addUris) {
        changeUri (gid, fileIndex, delUris, addUris, 0);
    }

    /*
     * This method returns options of the download denoted by gid. The response is of type struct.
     * Its key is the name of option. The value type is string.
     */
    public void getOptions(int gid, AbstractTask.Priority priority) {
        List<Object> list = new ArrayList<Object>();
        list.add( String.valueOf(gid) );

        GetOptionsTask task = new GetOptionsTask(list);
        task.setDownloadGid(gid);
        
        addTask(task);
        //OptionsParser
    }

    public void getOptions(int gid) {
        getOptions(gid, Task.Priority.FOREGROUND);
    }

    /*
     * This method changes options of the download denoted by gid dynamically. gid is of type string.
     * Options is of type struct and the available options are: bt-max-peers, bt-request-peer-speed-limit,
     * max-download-limit and max-upload-limit. This method returns "OK" for success.
     */
    public void changeOption (int gid, Options options) {
        throw new RuntimeException("Method is not implemented!");
    }

    /*
     * This method returns global options. The response is of type struct. Its key is the name of option.
     * The value type is string. Because global options are used as a template for the options of newly added download,
     * the response contains keys returned by aria2.getOption method.
     */
    public Options getGlobalOption() {
        //
        return new Options(new Object());
    }

    /*
     * This method changes global options dynamically. options is of type struct and the available options are
     * max-concurrent-downloads, max-overall-download-limit, max-overall-upload-limit, log-level and log.
     * Using log option, you can dynamically start logging or change log file. To stop logging, give empty string("")
     * as a parameter value. Note that log file is always opened in append mode. This method returns "OK" for success.
     */
    public void changeGlobalOption (Options options) {
        // Looks like we need another entity - Options (plural)
    }

    /*
     * This method purges completed/error/removed downloads to free memory. This method returns "OK".
     */
    public void purgeDownloadResult() {

        PurgeDownloadResultTask task = new PurgeDownloadResultTask();
        addTask(task);
        //StringParser
    }

    /*
     * This method removes completed/error/removed download denoted by gid from memory.
     * This method returns "OK" for success.
     */
    public void removeDownloadResult(int gid) {
        List<Object> list = new ArrayList<Object>();
        list.add( String.valueOf(gid) );
        
        RemoveDownloadResultTask task = new RemoveDownloadResultTask(list);
        task.setDownloadGid(gid);
        
        addTask(task);
        //StringParser
    }

    /*
     * This method returns version of the program and the list of enabled features.
     * The response is of type struct and contains following keys.
     */
    public void getVersion() {
        
        GetVersionTask task = new GetVersionTask();
        addTask(task);
        //StringParser
    }

    /*
     * This method returns session information. The response is of type struct and contains following key.
     */
    public void getSessionInfo() {
        //
    }

    /*
     * This method shutdowns aria2. This method returns "OK".
     */
    public void shutdown() {
        //
    }

    /*
     * This method shutdowns aria2. This method behaves like aria2.shutdown except that any actions which takes time
     * such as contacting BitTorrent tracker are skipped. This method returns "OK".
     */
    public void forceShutdown() {
        //
    }
    

    private void addTask(Task task) {
        TaskQueue.getInstance().addTask(task);
    }
    
}
