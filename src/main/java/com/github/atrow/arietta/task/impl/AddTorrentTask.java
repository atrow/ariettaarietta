package com.github.atrow.arietta.task.impl;

import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.DownloadAddedEvent;

public class AddTorrentTask extends AbstractTask {

    public AddTorrentTask(List<Object> params) {
        super(Task.METHOD_ADD_TORRENT, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {
        
        Download download = getDownloadResult();
        EventBusFactory.getDefault().fire(new DownloadAddedEvent(download));
    }

}
