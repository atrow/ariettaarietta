package com.github.atrow.arietta.task.parser;

import com.github.atrow.arietta.exception.InvalidRpcDataException;


public class StringParser {

    public static String parseStringObject(Object object) throws InvalidRpcDataException {

        if (object == null) {
            return null;
        }

        if (object instanceof String) {
            return (String) object;

        } else {
            throw new InvalidRpcDataException("Expected String, but got: " + object.getClass().getName());
        }
    }
}
