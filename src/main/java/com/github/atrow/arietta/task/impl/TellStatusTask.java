package com.github.atrow.arietta.task.impl;

import java.util.Arrays;
import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.DownloadsFetchedEvent;

public class TellStatusTask extends AbstractTask {

    public TellStatusTask(List<Object> params) {
        super(Task.METHOD_TELL_ACTIVE, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {
        
        List<Download> downloads = Arrays.asList(getDownloadResult());
        EventBusFactory.getDefault().fire(new DownloadsFetchedEvent(downloads));
    }

}
