package com.github.atrow.arietta.task;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import com.github.atrow.arietta.Arietta;
import com.github.atrow.arietta.Logger;
import com.github.atrow.arietta.Options;
import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.exception.ConnectionLostException;
import com.github.atrow.arietta.exception.InvalidResponseException;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.parser.DownloadListParser;
import com.github.atrow.arietta.task.parser.DownloadParser;
import com.github.atrow.arietta.task.parser.IntegerListParser;
import com.github.atrow.arietta.task.parser.IntegerParser;
import com.github.atrow.arietta.task.parser.OptionsParser;
import com.github.atrow.arietta.task.parser.StringParser;

/**
 * TODO: Write JavaDoc
 */

public abstract class AbstractTask implements Task {

    private final String method;
    private final List<Object> params;
    private final AbstractTask.Priority priority;
    private Object rawExecutionResult;

    protected AbstractTask(String method, List<Object> params, Priority priority) {
        this.method = method;
        this.priority = priority;
        this.params = params;
    }

    protected AbstractTask(String method, Priority priority) {
        this(method, new ArrayList<Object>(0), priority);
    }

    protected AbstractTask(String method) {
        this(method, Priority.FOREGROUND);
    }

    protected AbstractTask(String method, List<Object> params) {
        this(method, params, Priority.FOREGROUND);
    }

    @Override
    public int compareTo(Task otherTask) {
        return this.getTaskPriority().compareTo(otherTask.getTaskPriority());
    }

    /* (non-Javadoc)
     * @see com.github.atrow.arietta.task.Task#getTaskPriority()
     */
    @Override
    public AbstractTask.Priority getTaskPriority() {
        return priority;
    }

    // public Event getResultEvent(Object result) {
    // return EventFactory.getNewEventForMethod(getMethod(), result);
    // }

    private String getMethod() {
        return method;
    }

    private List<Object> getParams() {
        return params;
    }

    /* (non-Javadoc)
     * @see com.github.atrow.arietta.task.Task#handleResult()
     */
    @Override
    public abstract void handleResult() throws InvalidRpcDataException;

    // private void handleResult(Object result) {
    // Event event = getResultEvent(result);
    // EventBusFactory.getDefault().fire(event);
    // }

    @Override
    public String toString() {
        return "Task [method=" + method + ", params=" + params + ", priority=" + priority + "]";
    }

    /* (non-Javadoc)
     * @see com.github.atrow.arietta.task.Task#execute()
     */
    @Override
    public void execute() {
        Logger.getInstance().info("Executing method " + getMethod());

        try {
            rawExecutionResult = Arietta.rpc.callMethod(getMethod(), getParams());

            if (rawExecutionResult == null) {
                Logger.getInstance().error("Got null result for " + getMethod());
                throw new InvalidResponseException("Got null result");
            }

            Logger.getInstance().debug("Got result for " + getMethod());

            runInNewThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        handleResult();
                    } catch (InvalidRpcDataException e) {
                        Arietta.handleRemoteException(e);
                    }
                }
            });

        } catch (InvalidResponseException e) {
            Arietta.handleRemoteException(e);
        } catch (ConnectionLostException e) {
            Arietta.handleConnectException(e);
        }

        /**
         * If handleResult here will add another task to queue, RPC thread may
         * be deadlocked by itself, so let's run this method in EventQueue
         * thread.
         * 
         * EventQueue.invokeLater(new Runnable() {
         * 
         * @Override public void run() { try { handleResult(rawExecutionResult);
         *           } catch (ClassCastException e) {
         *           Arietta.handleException(e); } } });
         */
    }
    
    protected Object getRawExecutionResult() {
        return rawExecutionResult;
    }
    
    protected Integer getGidResult() throws InvalidRpcDataException {
        return IntegerParser.parseNumericStringObject(rawExecutionResult);
    }
    
    protected List<Download> getDownloadListResult() throws InvalidRpcDataException {
        return DownloadListParser.parseDownloadListObject(rawExecutionResult);
    }
    
    protected Download getDownloadResult() throws InvalidRpcDataException {
        return DownloadParser.parseDownloadObject(rawExecutionResult);
    }

    protected List<Integer> getGitListResult() throws InvalidRpcDataException {
        return IntegerListParser.parseNumericStringArrayObject(rawExecutionResult);
    }
    
    protected Options getOptionsResult() throws InvalidRpcDataException {
        return OptionsParser.parseOptionsObject(rawExecutionResult);
    }
    
    protected String getStringResult() throws InvalidRpcDataException {
        return StringParser.parseStringObject(rawExecutionResult);
    }

    // TODO: don't use EventQueue thread
    private void runInNewThread(Runnable runnable) {
        EventQueue.invokeLater(runnable);
    }
    
}
