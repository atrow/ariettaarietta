package com.github.atrow.arietta.task.parser;

import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class IntegerParser {

    public static Integer parseNumericStringObject(Object object) throws InvalidRpcDataException {

        if (object == null) {
            return null;
        }

        if (object instanceof String) {
            return parseNumericString((String) object);

        } else {
            throw new InvalidRpcDataException("Expected numeric String, but got: " + object.getClass().getName());
        }
    }

    public static Integer parseNumericString(String string) throws InvalidRpcDataException {
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {

            if (string == null) {
                return null;
            } else {
                throw new InvalidRpcDataException("Expected numeric String, but got: " + string);
            }
        }
    }

}