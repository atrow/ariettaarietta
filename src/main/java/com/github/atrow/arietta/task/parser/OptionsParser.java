package com.github.atrow.arietta.task.parser;

import com.github.atrow.arietta.Options;
import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class OptionsParser {
    
    public static Options parseOptionsObject(Object result) throws InvalidRpcDataException {
        return new Options(result);
    }
}