package com.github.atrow.arietta.task.impl;

import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.DownloadListAddedEvent;

public class AddMetalinkTask extends AbstractTask {

    public AddMetalinkTask(List<Object> params) {
        super(Task.METHOD_ADD_METALINK, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {
        
        List<Download> downloads = getDownloadListResult();
        EventBusFactory.getDefault().fire(new DownloadListAddedEvent(downloads));
    }

}
