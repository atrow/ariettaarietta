package com.github.atrow.arietta.task.impl;

import java.util.List;

import com.github.atrow.arietta.Options;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.OptionsFetchedEvent;

public class GetOptionsTask extends AbstractTask {

    private Integer downloadGid;

    public GetOptionsTask(List<Object> params) {
        super(Task.METHOD_GET_OPTION, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {
        
        Options options = getOptionsResult();
        EventBusFactory.getDefault().fire(new OptionsFetchedEvent(options));            
    }

    public Integer getDownloadGid() {
        return downloadGid;
    }

    public void setDownloadGid(Integer downloadGid) {
        this.downloadGid = downloadGid;
    }

}
