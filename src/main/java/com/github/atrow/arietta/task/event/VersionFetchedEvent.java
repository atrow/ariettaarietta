/**
 * 
 */
package com.github.atrow.arietta.task.event;

import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class VersionFetchedEvent implements Event {

    private String version;
    
    public VersionFetchedEvent(String version) {
        this.version = version;
    }

    String getVersion() {
        return version;
    }
  
    public static abstract class Listener extends AbstractEventListener<VersionFetchedEvent> {

        @Override
        public abstract void handle(VersionFetchedEvent event);

        @Override
        public boolean canHandle(Event event) {
            return (event instanceof VersionFetchedEvent);
        }
        
    }
    
}
