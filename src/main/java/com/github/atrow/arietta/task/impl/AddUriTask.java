package com.github.atrow.arietta.task.impl;

import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.DownloadAddedEvent;

public class AddUriTask extends AbstractTask {

    private String downloadName;

    public AddUriTask(List<Object> params) {
        super(Task.METHOD_ADD_URI, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {

        Download download = getDownloadResult();
        if (downloadName != null) {
            download.setName(downloadName);
        }

        EventBusFactory.getDefault().fire(new DownloadAddedEvent(download));
    }

    public void setDownloadName(String name) {
        this.downloadName = name;
    }

}
