package com.github.atrow.arietta.task.impl;

import java.util.List;

import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;
import com.github.atrow.arietta.task.AbstractTask;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.event.DownloadRemovedEvent;

public class PauseTask extends AbstractTask {

    public PauseTask(List<Object> params) {
        super(Task.METHOD_PAUSE, params);
    }

    @Override
    public void handleResult() throws InvalidRpcDataException {

        Integer downloadGid = getGidResult();
        EventBusFactory.getDefault().fire(new DownloadRemovedEvent(downloadGid));
    }

}
