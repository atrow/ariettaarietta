/**
 * 
 */
package com.github.atrow.arietta.task.event;

import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class DownloadListAddedEvent implements Event {
    
    private final List<Download> downloads;
    
    public DownloadListAddedEvent(List<Download> downloads) {
        this.downloads = downloads;
    }
    
    public List<Download> getDownload() {
        return downloads;
    }

    public static abstract class Listener extends AbstractEventListener<DownloadListAddedEvent> {

        @Override
        public abstract void handle(DownloadListAddedEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof DownloadListAddedEvent);
        }
        
    }
    
}