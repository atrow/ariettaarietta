package com.github.atrow.arietta.task.parser;

import java.util.ArrayList;
import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.download.DownloadFactory;
import com.github.atrow.arietta.exception.InvalidRpcDataException;

public class DownloadListParser {

    public static List<Download> parseDownloadListObject(Object result) throws InvalidRpcDataException {

        List<Download> list = null;

        if (result instanceof Object[]) {

            Object[] objects = (Object[]) result;
            list = new ArrayList<Download>(objects.length);

            for (Object object : objects) {
                list.add(DownloadFactory.createNewDownload(object));
            }

        } else {
            if (result == null) {
                throw new IllegalArgumentException("Expected Download info maps, but got null");
            } else {
                throw new IllegalArgumentException("Expected Download info maps, but got: "
                        + result.getClass().getName());
            }
        }

        return list;
    }
}