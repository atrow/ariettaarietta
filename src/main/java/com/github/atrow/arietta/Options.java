package com.github.atrow.arietta;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for managing Download options
 * TODO: There's much work should be done here. Write JavaDoc then.
 */
public class Options {

    public Options(Object result) {
        Logger.getInstance().error("Result: ", result);
        Map<String, String> resmap = (Map<String, String>)result;

        for (String key : resmap.keySet()) {
            String value = resmap.get(key);

            System.out.println(key + " " + value);
        }

    }

    public Options() {
        max_download_limit = "20K";
    }

    /**
     * @return Options as Map, to be sent trough RPC
     */
    public Map<String, String> serialize() {
        Map<String, String> map = new HashMap<String, String>();

        map.put("dir", "/storage/Downloads/Arietta/2");
        //map.put("max-download-limit", max_download_limit);
        //map.put("max-concurrent-downloads", "1");

        return map;
    }

    public String max_download_limit;

    /*
     * The directory to store the downloaded file.
     */
    public String dir;

    /*
     * Downloads URIs found in FILE. You can specify multiple URIs for a single entity: separate URIs on a single line
     * using the TAB character. Reads input from stdin when - is specified. Additionally, options can be specified after
     * each line of URI. This optional line must start with one or more white spaces and have one option per single line.
     * See Input File subsection for details.
     */
    public String input_file;

    /*
     * The file name of the log file. If - is specified, log is written to stdout. If empty string("") is specified,
     * log is not written to file.
     */

    public String log;

    /*
     * Set maximum number of parallel downloads for every static (HTTP/FTP) URI, torrent and metalink.
     * See also -s and -C option. Default: 5
     */
    public int max_concurrent_downloads = 5;

    /*
     * Check file integrity by validating piece hashes or a hash of entire file. This option has effect only in
     * BitTorrent and Metalink downloads with checksums. If piece hashes are provided, this option can detect damaged
     * portions of a file and re-download them. If a hash of entire file is provided, hash check is only done when file
     * has benn already download. This is determined by file length. If hash check fails, file is re-downloaded from scratch.
     * If both piece hashes and a hash of entire file are provided, only piece hashes are used. Default: false
     */
    public boolean check_integrity = false;

    /*
     * Continue downloading a partially downloaded file. Use this option to resume a download started by a web browser or
     * another program which downloads files sequentially from the beginning. Currently this option is only applicable
     * to HTTP(S)/FTP downloads.
     */
    public boolean continue_on;

    /*
     * The help messages are classified with tags. A tag starts with "#". For example, type "--help=#http" to get the
     * usage for the options tagged with "#http". If non-tag word is given, print the usage for the options whose name
     * includes that word. Available Values: #basic, #advanced, #http, #https, #ftp, #metalink, #bittorrent, #cookie,
     * #hook, #file, #rpc, #experimental, #all Default: #basicThe help messages are classified with tags. A tag starts
     * with "#". For example, type "--help=#http" to get the usage for the options tagged with "#http". If non-tag word
     * is given, print the usage for the options whose name includes that word. Available Values: #basic, #advanced,
     * #http, #https, #ftp, #metalink, #bittorrent, #cookie, #hook, #file, #rpc, #experimental, #all Default: #basic
     */
    // help[=TAG|KEYWORD]

}
