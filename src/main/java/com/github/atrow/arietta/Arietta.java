package com.github.atrow.arietta;

import com.github.atrow.arietta.event.EventBus;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.event.impl.ConnectionLostEvent;
import com.github.atrow.arietta.rpc.RPC;
import com.github.atrow.arietta.task.BackgroundTicker;
import com.github.atrow.arietta.task.event.OptionsFetchedEvent;
import com.github.atrow.arietta.ui.UI;
import com.github.atrow.arietta.ui.view.ErrorDialog;


/**
 * Main class of the application, acts as Controller.
 */
public class Arietta {

    /**
     * Single instance of Interface, encapsulates all GUI and related tasks
     */
    private static UI ui;

    /**
     * Default options for all downloads
     * TODO: Maybe, there is no need in it
     */
    private static Options defaultOptions;

    /**
     * Single instance of RPC, which performs all interaction with aria2 server
     */
    public static RPC rpc;

    //
    // Main method f the application
    //
    public static void main(String[] args) {

        try {

            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    handleException(e);
                }
            });

            setupEventListeners();
            
            BackgroundTicker.getInstance();
            DownloadManager.getInstance();
            
            defaultOptions = new Options();
            ui = new UI();
            rpc = new RPC();

            Logger.getInstance().info("Arietta started");

        } catch (Throwable e) {
            handleException(e);
        }

    }
    
    private static void setupEventListeners() {
        
        EventBus bus = EventBusFactory.getDefault();
        
        bus.registerEventListener(new OptionsFetchedEvent.Listener() {
            @Override
            public void handle(OptionsFetchedEvent event) {
                // TODO: Implement handling
            }
        });

       
    }


    // Public methods
    //
    // Getters
    //

    /**
     * @return Current default options
     */
    public static Options getDefaultOptions() {
        return defaultOptions;
    }

    //
    // Main action methods
    //
    
    /**
     * Get states of all downloads.
     * It's assumed that this application doesn't have MONOPOLY control over aria2, so anything on server can be changed
     * without notifying. That's why we should fetch ALL information, and update internal state basing on it.
     
    public static void updateAll() {

        //TODO: Bug here, fix later
        Date now = new Date();
        if ((now.getTime() - lastUpdateAll) < ((double)Config.getInstance().getUpdateInterval() / 1000)) {
            lastUpdateAll = now.getTime();
            return;
        }

        // Download maniacs can have problems here
        int num = 1000;
        int offset = 0;

        TaskManager manager = TaskManager.getInstance();
        
        manager.tellActive();
        manager.tellWaiting(offset, num);
        manager.tellStopped(offset, num);

        //TODO: Find how to update information about downloads which where removed from aria2 in another way
    }
*/

    //
    // Custom error handling
    //

    /**
     * Fires when WindowConfig can't save preferences
     * TODO: Remake behavior of exceptions. Here should be WindowConfig.PreferencesStorageDeniedException
     */
    public static void onPreferencesStorageDenied() {
        throw new RuntimeException("Preferences Storage Denied");
    }

    /**
     * Method should be called when exception signals about lost of connection
     * @param e Accept any Throwable for compatibility
     */
    public static void handleConnectException(Throwable e) {

        EventBusFactory.getDefault().fire(new ConnectionLostEvent());

        String msg = e.getLocalizedMessage() + "\n\n"+"Check settings?";

        ErrorDialog.Callback cb = new ErrorDialog.Callback() {
            @Override
            public void onOK() { ui.showSettings(); }
            @Override
            public void onCancel() {}
        };

        ui.showError(cb, "Connection Error", msg, ErrorDialog.ButtonSet.OK_CANCEL);
    }

    /**
     * Method should be called when we need to show aria2 error and continue work
     * @param e RemoteException throwed on RPC interaction
     */
    public static void handleRemoteException(Throwable e) {

        //String msg = e.getCause().getCause().getLocalizedMessage();
        String msg = e.getLocalizedMessage();

        ErrorDialog.Callback cb = new ErrorDialog.Callback() {
            @Override
            public void onOK() {}
            @Override
            public void onCancel() {}
        };

        ui.showError(cb, "Aria2 Error", msg, ErrorDialog.ButtonSet.OK);
    }

    /**
     * Method should be called when unexpected exception occurs
     * @param e Any Throwable
     */
    public static void handleException(Throwable e) {
        String em = e.getLocalizedMessage();
        Logger.getInstance().error(em, e);

        ErrorDialog.Callback cb = new ErrorDialog.Callback() {
            @Override
            public void onOK() {}
            @Override
            public void onCancel() { System.exit(0); }
        };

        ui.showError(cb, "Error", em+"\n\n Click OK to continue, Cancel to exit", ErrorDialog.ButtonSet.OK_CANCEL);
    }
}
