/**
 * 
 */
package com.github.atrow.arietta.event.impl;

import com.github.atrow.arietta.StateFilter;
import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class StateFilterChangedEvent implements Event {
    
    private StateFilter stateFilter;
    
    public StateFilterChangedEvent(StateFilter newStateFilter) {
        this.stateFilter = newStateFilter;
    }
    
    public StateFilter getStateFilter() {
        return stateFilter;
    }

    public static abstract class Listener extends AbstractEventListener<StateFilterChangedEvent> {

        @Override
        public abstract void handle(StateFilterChangedEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof StateFilterChangedEvent);
        }
        
    }
    
}
