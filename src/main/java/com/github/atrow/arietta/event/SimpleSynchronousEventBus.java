/**
 * 
 */
package com.github.atrow.arietta.event;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Very plain and simple synchronous EventBus. May be remade or extended.
 * 
 * Stores all EventListeners in ArrayList collection, 
 * which means no duplicate checking and preservation of order.
 *
 * Every Event will be handled by EventListeners as many times, 
 * as many appropriate EventListeners are registered, including duplicates.
 * 
 * All the handling process is single-treaded, i.e., synchronous.
 */
public class SimpleSynchronousEventBus implements EventBus {

    private Collection<EventListener<? extends Event>> listeners = new ArrayList<EventListener<? extends Event>>();
    
    @Override
    public synchronized void registerEventListener(EventListener<? extends Event> listener) {
        listeners.add(listener);
    }

    @Override
    public synchronized void unRegisterEventListener(EventListener<? extends Event> listener) {
        listeners.remove(listener);
    }

    @Override
    public synchronized void fire(Event event) {
        for (EventListener<? extends Event> listener : listeners) {
            if (listener.canHandle(event)) {
                listener.handleGeneric(event);
            }
        }
    }

}
