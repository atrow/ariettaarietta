/**
 * 
 */
package com.github.atrow.arietta.event.impl;

import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class ConfigUpdatedEvent implements Event {

    public static abstract class Listener extends AbstractEventListener<ConfigUpdatedEvent> {

        @Override
        public abstract void handle(ConfigUpdatedEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof ConfigUpdatedEvent);
        }
        
    }
    
}
