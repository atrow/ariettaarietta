/**
 * 
 */
package com.github.atrow.arietta.event.impl;

import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class ConnectionEstablishedEvent implements Event {

    public static abstract class Listener extends AbstractEventListener<ConnectionEstablishedEvent> {

        @Override
        public abstract void handle(ConnectionEstablishedEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof ConnectionEstablishedEvent);
        }
        
    }
    
}
