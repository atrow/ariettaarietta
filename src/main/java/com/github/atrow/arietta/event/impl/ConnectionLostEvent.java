/**
 * 
 */
package com.github.atrow.arietta.event.impl;

import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class ConnectionLostEvent implements Event {

    public static abstract class Listener extends AbstractEventListener<ConnectionLostEvent> {

        @Override
        public abstract void handle(ConnectionLostEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof ConnectionLostEvent);
        }
        
    }
    
}
