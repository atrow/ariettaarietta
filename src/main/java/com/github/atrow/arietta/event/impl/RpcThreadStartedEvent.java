/**
 * 
 */
package com.github.atrow.arietta.event.impl;

import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class RpcThreadStartedEvent implements Event {

    public static abstract class Listener extends AbstractEventListener<RpcThreadStartedEvent> {

        @Override
        public abstract void handle(RpcThreadStartedEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof RpcThreadStartedEvent);
        }
        
    }
    
}
