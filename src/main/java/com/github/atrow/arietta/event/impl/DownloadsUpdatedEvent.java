/**
 * 
 */
package com.github.atrow.arietta.event.impl;

import java.util.List;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class DownloadsUpdatedEvent implements Event {
    
    private final List<Download> downloads;
    
    public DownloadsUpdatedEvent(List<Download> downloads) {
        this.downloads = downloads;
    }

    public List<Download> getDownloads() {
        return downloads;
    }
    
    public static abstract class Listener extends AbstractEventListener<DownloadsUpdatedEvent> {

        @Override
        public abstract void handle(DownloadsUpdatedEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof DownloadsUpdatedEvent);
        }
        
    }
    
}
