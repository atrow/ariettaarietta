package com.github.atrow.arietta.event;

/**
 * Should be registered in the event bus. 
 * When the Event is fired to this bus, this Listener will receive it
 * 
 * @param <T> Class of the concrete Event type, which this Listener handles
 */
public interface EventListener<T extends Event> {

    /**
     * Implemented in AbstractEventListener.
     * Accepts generic Event, but handles it as concrete Event type
     * 
     * @param event Event to handle
     */
    void handleGeneric(Event event);
    
    /**
     * Must be implemented by concrete EventListener.
     * Perform specific operations with concrete Event type <T>
     * 
     * @param event Event to handle 
     */
    void handle(T event);

    /**
     * Must be implemented by concrete EventListener.
     * Determine whether this Event can (should) be handled by this Listener.
     * If returns true, the Listener will receive this Event to handleGeneric() method
     * 
     * @param event Event to check
     * @return true if Listener can and will handle this Event
     */
    boolean canHandle(Event event);
    
}
