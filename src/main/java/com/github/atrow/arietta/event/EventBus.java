package com.github.atrow.arietta.event;

/**
 * Simple EventBus definition.
 * Allows registration of EventListeners and firing Events.
 *
 */
public interface EventBus {

    /**
     * Make possible an EventListener to receive Events passed in the EventBus
     * @param listener
     */
    void registerEventListener(EventListener<? extends Event> listener);
    
    /**
     * Disconnect an EventListener from receiving Events passed in the EventBus
     * @param listener
     */
    void unRegisterEventListener(EventListener<? extends Event> listener);
    
    /**
     * Pass the Event to EventBus to be handled by EventListener 
     * @param event
     */
    void fire(Event event);
    
}
