/**
 * 
 */
package com.github.atrow.arietta.event.impl;

import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class QueueReadyEvent implements Event {

    public static abstract class Listener extends AbstractEventListener<QueueReadyEvent> {

        @Override
        public abstract void handle(QueueReadyEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof QueueReadyEvent);
        }
        
    }
    
}
