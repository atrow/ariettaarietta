/**
 * 
 */
package com.github.atrow.arietta.event.impl;

import com.github.atrow.arietta.event.AbstractEventListener;
import com.github.atrow.arietta.event.Event;

/**
 *
 */
public class QueueFullEvent implements Event {

    public static abstract class Listener extends AbstractEventListener<QueueFullEvent> {

        @Override
        public abstract void handle(QueueFullEvent event);
        
        @Override
        public boolean canHandle(Event event) {
            return (event instanceof QueueFullEvent);
        }
        
    }
    
}
