package com.github.atrow.arietta.event;


/**
 * Provides default implementation of handleGeneric() method
 *
 * @param <T> Class of the concrete Event type, which this Listener handles
 */
public abstract class AbstractEventListener<T extends Event> implements EventListener<T> {

    @SuppressWarnings("unchecked")
    @Override
    public void handleGeneric(Event event) {
        assert canHandle(event);// : "You must do a check with canHandle() first";
        handle( (T) event );
    }

}
