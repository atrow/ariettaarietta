package com.github.atrow.arietta.event;

public class EventBusFactory {

    private static EventBus eventBus;
    
    public static EventBus getDefault() {
        if (eventBus == null) {
            eventBus = createDefaultEventBus();
        }
        return eventBus;
    }

    private static EventBus createDefaultEventBus() {
        return new SimpleSynchronousEventBus();
    }    
    
}
