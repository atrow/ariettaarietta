package com.github.atrow.arietta.rpc;

import java.rmi.RemoteException;
import java.util.List;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.common.TypeFactoryImpl;
import org.apache.xmlrpc.common.XmlRpcController;
import org.apache.xmlrpc.common.XmlRpcStreamConfig;
import org.apache.xmlrpc.serializer.StringSerializer;
import org.apache.xmlrpc.serializer.TypeSerializer;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import com.github.atrow.arietta.Config;
import com.github.atrow.arietta.Logger;
import com.github.atrow.arietta.event.EventBus;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.event.EventListener;
import com.github.atrow.arietta.event.impl.ConfigUpdatedEvent;
import com.github.atrow.arietta.event.impl.ConnectionEstablishedEvent;
import com.github.atrow.arietta.event.impl.RpcThreadStartedEvent;
import com.github.atrow.arietta.exception.ConnectionLostException;
import com.github.atrow.arietta.exception.InvalidResponseException;
import com.github.atrow.arietta.task.Task;
import com.github.atrow.arietta.task.TaskQueue;

/*

All RPC methods in aria2:

* ADD:
  - HTTP(S)/FTP/BitTorrent Magnet URI
  - .torrent file
  - .metalink file


* remove
* pause
* unpause
* tellStatus
* changePosition
* changeUri

* getFiles
* getPeers
* getServers

* tellActive
* tellWaiting
* tellStopped

* purgeDownloadResult
* removeDownloadResult

* getOption
* changeOption
* getGlobalOption
* changeGlobalOption

* getVersion
* getSessionInfo
* shutdown
* forceShutdown

** multicall
 */

/**
 * RPC class. Establishes connection to server. Runs calls, handles responses.
 */
public class RPC {

    /**
     * Instance of org.apache.xmlrpc.client
     * All RPC calls goes through this client
     */
    public XmlRpcClient client;

    /**
     * WindowConfig for client
     */
    XmlRpcClientConfigImpl clientConfig;

    /**
     * Tasks from queue are handled by this single dedicated thread
     */
    // TODO: Need other notification method
    private Thread thread;

    /**
     * 
     */
    private EventBus eventBus = EventBusFactory.getDefault();
    
    /**
     * Constructor
     */
    public RPC() {

        setupEventListeners();
        
        /**
         * Thread will endlessly call executeNext() method
         */
        // TODO: Move out from RPC
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        // TODO: run CheckVersion to determine connection state, and only then fire CE event
                        Logger.getInstance().info("RPC thread (re)started");
                        EventBusFactory.getDefault().fire(new RpcThreadStartedEvent());
                        EventBusFactory.getDefault().fire(new ConnectionEstablishedEvent());

                        while (true) {
                            Logger.getInstance().info("executeNext");
                            executeNext();
                        }
                    } catch (InterruptedException e) {
                        // Do nothing. Just continue loop.
                    }
                }
            }
        }, "RPC worker");


        clientConfig = new XmlRpcClientConfigImpl();
        client = new XmlRpcClient();
        client.setTypeFactory(new ESTypeFactoryImpl(client));
        loadConfig();

        //TODO: is it needed?
        //thread.setDaemon(true);
        thread.start();
    }

    private void setupEventListeners() {
        EventListener<ConfigUpdatedEvent> configUpdatedEventListener = new ConfigUpdatedEvent.Listener() {
            
            @Override
            public void handle(ConfigUpdatedEvent event) {
                loadConfig();
            }
        };
        
        eventBus.registerEventListener(configUpdatedEventListener);
    }

    /**
     * Checks connection by executing RPC
     * @throws RemoteException When single RPC operation can't be done
     */
    public void checkConnection() throws RemoteException {
        //getVersion();
    }

    /**
     * Loads RPC options from WindowConfig
     */
    public void loadConfig() {
        if (Config.getInstance().getRpcURL() != null) {
            clientConfig.setServerURL( Config.getInstance().getRpcURL() );
            clientConfig.setBasicUserName( Config.getInstance().getRpcUser() );
            clientConfig.setBasicPassword( Config.getInstance().getRpcPass() );
            client.setConfig(clientConfig);
        }
    }

    /**
     * Method puts thread in sleep and waits for interrupt and non-empty queue.
     * Then it executes single task from queue.
     * @throws InterruptedException 
     * @throws RPC.NullReturnedException When aria2 returns null response
     * @throws java.rmi.RemoteException When aria2 sends Error message
     * @throws InvalidResponseException When handler cannot parse returned data
     */
    // TODO: Move out from RPC
    private void executeNext() throws InterruptedException {

        Logger.getInstance().info("Fetching next task from queue");

        Task task = TaskQueue.getInstance().getTaskOrWait();
        task.execute();
    }

    public Object callMethod(String method, List<Object> params) throws InvalidResponseException, ConnectionLostException {
        try {
            return client.execute(method, params);
        } catch (XmlRpcException xe) {
            switch (xe.code) {
                //Non-fatal exception
                case 1: throw new InvalidResponseException("Aria2 Error", xe);

                //Connection lost exception
                case 0: throw new ConnectionLostException("Lost connection to Aria2", xe);

                //Unknown code
                default: throw new ConnectionLostException("Server error code ["+xe.code+"]", xe);
            }
        } catch (Exception e) {
        	throw new ConnectionLostException("Cannot execute command", e);
        }
    }

    static private class ExplicitStringSerializer extends StringSerializer {
        @Override
        public void write(ContentHandler pHandler, Object pObject) throws SAXException {
            write(pHandler, STRING_TAG, pObject.toString());
        }
    }

    static private class ESTypeFactoryImpl extends TypeFactoryImpl {
        public ESTypeFactoryImpl(XmlRpcController pController) {
            super(pController);
        }

        @Override
        public TypeSerializer getSerializer(XmlRpcStreamConfig pConfig, Object pObject) throws SAXException {
            if(pObject instanceof String) {
                return new ExplicitStringSerializer();
            } else {
                return super.getSerializer(pConfig, pObject);
            }
        }
    }

}

