package com.github.atrow.arietta;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.atrow.arietta.download.Download;
import com.github.atrow.arietta.event.EventBus;
import com.github.atrow.arietta.event.EventBusFactory;
import com.github.atrow.arietta.event.impl.ConnectionLostEvent;
import com.github.atrow.arietta.event.impl.DownloadsUpdatedEvent;
import com.github.atrow.arietta.event.impl.StateFilterChangedEvent;
import com.github.atrow.arietta.task.TaskManager;
import com.github.atrow.arietta.task.event.DownloadAddedEvent;
import com.github.atrow.arietta.task.event.DownloadListAddedEvent;
import com.github.atrow.arietta.task.event.DownloadRemovedEvent;
import com.github.atrow.arietta.task.event.DownloadsFetchedEvent;

public final class DownloadManager {

    private final TaskManager taskManager = TaskManager.getInstance();
    private final EventBus eventBus = EventBusFactory.getDefault();
    private StateFilter stateFilter = StateFilter.getStateFilterAll();

    private static DownloadManager instance;

    /**
     * Map, which store Download objects. The Key is GID of stored download, so
     * it can be easily accessed by it
     */
    private final Map<Integer, Download> downloads = new HashMap<Integer, Download>();

    private DownloadManager() {
        setupEventListeners();
    }

    public static synchronized DownloadManager getInstance() {
        if (instance == null) {
            instance = new DownloadManager();
        }
        return instance;
    }

    /**
     * Add download(s) from local file
     * 
     * @param fileName
     *            Full path to .torrent or .metalink file TODO: Maybe, this
     *            method should accept File instead of Sting
     */
    public void addDownloadFromFileName(String fileName) {
        try {
            byte[] content = FileReader.readByteArray(fileName);

            // TODO: Maybe, need real (but simple) check of file format
            if (content.length < 3) {
                throw new RuntimeException("Wrong file format");
            }

            if (fileName.toLowerCase().endsWith(".torrent"))
                taskManager.addTorrent(content);
            else if (fileName.toLowerCase().endsWith(".metalink"))
                taskManager.addMetalink(content);
        } catch (IOException e) {
            Arietta.handleException(e);
        }
    }

    /**
     * Remove specified download
     * 
     * @param gid
     *            GID of download to be removed (aria2 download id)
     */
    public void removeDownload(final int gid) {

        // Deletion of download is different for different sates
        switch (downloads.get(gid).getStatus()) {
        case COMPLETE:
        case ERROR:
        case REMOVED:
            taskManager.removeDownloadResult(gid);
            break;

        case NEW:
        case ACTIVE:
        case WAITING:
        case PAUSED:
        case UNKNOWN:
            taskManager.remove(gid);
            break;
        }

    }

    /**
     * Pause specified download
     * 
     * @param gid
     *            GID of download to be paused (aria2 download id)
     */
    public void pauseDownload(final int gid) {

        switch (downloads.get(gid).getStatus()) {
        case ACTIVE:
        case WAITING:
            taskManager.pause(gid);
            break;

        default:
            break;
        }
    }

    /**
     * UnPause specified download
     * 
     * @param gid
     *            GID of download to be unpaused (aria2 download id)
     */
    public void unpauseDownload(final int gid) {

        switch (downloads.get(gid).getStatus()) {
        case PAUSED:
            taskManager.unpause(gid);
            break;

        default:
            break;
        }
    }

    //
    // Getters
    //
    /**
     * Get Download by gid
     * 
     * @param gid
     *            GID (aria2 download id)
     * @return Download
     */
    public Download getDownload(int gid) {
        return downloads.get(gid);
    }

    /**
     * Returns Download Map with Integer key
     * 
     * @return Downloads
     */
    public Map<Integer, Download> getDownloadsMap() {
        return downloads;
    }

    /**
     * Returns List of all Download objects
     * 
     * @return Download list
     */
    public List<Download> getDownloadsListAll() {
        return new ArrayList<Download>(downloads.values());
    }

    /**
     * Returns List of Download objects, that matches specified StateFilter
     * 
     * @param filter
     *            Filter to bypass only required downloads
     * @return Filtered Download list
     */
    public List<Download> getDownloadsListFiltered() {
        ArrayList<Download> list = new ArrayList<Download>();

        for (Download download : downloads.values()) {
            if (stateFilter.contains(download.getStatus())) {
                list.add(download);
            }
        }

        return list;
    }


    // TODO: Following methods can be seriously changed! Write JavaDoc later

    private void removeFromList(int gid) {
        downloads.remove(gid);
        eventBus.fire(new DownloadsUpdatedEvent(getDownloadsListFiltered()));
    }

    private void addToList(Download download) {
        downloads.put(download.getGid(), download);
        eventBus.fire(new DownloadsUpdatedEvent(getDownloadsListFiltered()));
    }

    private void addToListAll(List<Download> downloadList) {
        for (Download download : downloadList) {
            downloads.put(download.getGid(), download);
        }
        eventBus.fire(new DownloadsUpdatedEvent(getDownloadsListFiltered()));
    }

    private void clearList() {
        downloads.clear();
        eventBus.fire(new DownloadsUpdatedEvent(getDownloadsListFiltered()));
    }

    private void updatedownload(Download download) {
        int gid = download.gid;

        if (downloads.containsKey(gid)) {
            downloads.put(gid, download);
        } else {
            addToList(download);
        }
    }

    private void updatedownloads(List<Download> downloadsList) {
        for (Download download : downloadsList) {
            updatedownload(download);
        }

        eventBus.fire(new DownloadsUpdatedEvent(getDownloadsListFiltered()));
    }

    private void setupEventListeners() {

        eventBus.registerEventListener(new StateFilterChangedEvent.Listener() {
            @Override
            public void handle(StateFilterChangedEvent event) {
                stateFilter = event.getStateFilter();
            }
        });

        eventBus.registerEventListener(new ConnectionLostEvent.Listener() {
            @Override
            public void handle(ConnectionLostEvent event) {
                clearList();
            }
        });

        eventBus.registerEventListener(new DownloadsFetchedEvent.Listener() {
            @Override
            public void handle(DownloadsFetchedEvent event) {
                List<Download> downloads = event.getDownloads();
                updatedownloads(downloads);
            }
        });

        eventBus.registerEventListener(new DownloadAddedEvent.Listener() {
            @Override
            public void handle(DownloadAddedEvent event) {
                addToList(event.getDownload());
            }
        });

        eventBus.registerEventListener(new DownloadListAddedEvent.Listener() {
            @Override
            public void handle(DownloadListAddedEvent event) {
                addToListAll(event.getDownload());
            }
        });

        eventBus.registerEventListener(new DownloadRemovedEvent.Listener() {
            @Override
            public void handle(DownloadRemovedEvent event) {
                removeFromList(event.getDownloadGid());
            }
        });

    }

}
