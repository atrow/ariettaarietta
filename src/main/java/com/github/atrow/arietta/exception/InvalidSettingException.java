package com.github.atrow.arietta.exception;

/**
 * Exception throwed when WindowConfig gets invalid value as parameter
 */
public class InvalidSettingException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1123473228380790407L;

	public InvalidSettingException() {
    }

    public InvalidSettingException(String message) {
        super(message);
    }

    public InvalidSettingException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidSettingException(Throwable cause) {
        super(cause);
    }
}