package com.github.atrow.arietta.exception;


public class InvalidResponseException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 8112548028526838882L;

    public InvalidResponseException() {
    }

    public InvalidResponseException(String message) {
        super(message);
    }

    public InvalidResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}