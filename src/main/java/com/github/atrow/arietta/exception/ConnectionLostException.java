package com.github.atrow.arietta.exception;


public class ConnectionLostException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1769066838467502650L;

    public ConnectionLostException() {
    }

    public ConnectionLostException(String message) {
        super(message);
    }

    public ConnectionLostException(String message, Throwable cause) {
        super(message, cause);
    }
}