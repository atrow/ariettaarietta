package com.github.atrow.arietta.exception;


public class InvalidRpcDataException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 8112548028526838882L;

    public InvalidRpcDataException() {
    }

    public InvalidRpcDataException(String message) {
        super(message);
    }

    public InvalidRpcDataException(String message, Throwable cause) {
        super(message, cause);
    }
}