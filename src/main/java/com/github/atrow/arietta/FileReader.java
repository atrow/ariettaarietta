package com.github.atrow.arietta;

import java.io.*;

/**
 * Class that operates with files contents
 * TODO: Need normal error handling
 */
public class FileReader {

    /**
     * Reads binary file and returns array of byte
     * @param fileName Name of file
     * @return Binary file data
     * @throws IOException When something goes wrong
     */
    public static byte[] readByteArray(String fileName) throws IOException {
        File f = new File(fileName);

        if (!(f.exists() && f.canRead())) {
            throw new FileNotFoundException(fileName+" is not found");
        }

        byte[] data = new byte[ (int)f.length() ];
        InputStream inStream = new FileInputStream(f);

        try {
            inStream.read(data);
        } finally {
            inStream.close();
        }

        return data;
    }

}
